//                              -*- Mode: Verilog -*-
// Filename        : spi_master.sv
// Description     : The module connects Mentor's SPI Master QVIP with DUT slaves.
//
// Author          : Adrian Fiergolski
// Created On      : Fri May 3 12:49:53 2019
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//The module connects Mentor's SPI Master QVIP with DUT slaves.
//Inspired by Mentor's module.
module spi_master # (
                     int    SPI_SS_WIDTH = 32,
                     string IF_NAME = "null",
                     string PATH_NAME ="uvm_test_top*")
   (
    input 			  sys_clk,
    input 			  reset,
   
    output [(SPI_SS_WIDTH-1) : 0] SS,
    output 			  SCK,

    input 			  MISO,
    input 			  MISO1,
    input 			  MISO2,
    input 			  MISO3,

   
    output 			  MOSI,
    output 			  MOSI1,
    output 			  MOSI2,
    output 			  MOSI3

    );

   import uvm_pkg::*;

   typedef virtual 		  mgc_spi # (SPI_SS_WIDTH) spi_if_t;

   mgc_spi #(SPI_SS_WIDTH) spi_if (sys_clk);

   assign MOSI  = spi_if.MOSI;
   assign MOSI1 = spi_if.MOSI1;
   assign MOSI2 = spi_if.MOSI2;
   assign MOSI3 = spi_if.MOSI3;
   
   assign spi_if.RESET = reset;
   assign spi_if.MISO  = MISO;
   assign spi_if.MISO1 = MISO1;
   assign spi_if.MISO2 = MISO2;
   assign spi_if.MISO3 = MISO3;

   assign SS  = spi_if.SS;
   assign SCK = spi_if.SCK;

   initial
     begin
	uvm_config_db #( spi_if_t )::set(null, PATH_NAME, IF_NAME, spi_if);
     end

endmodule
