//                              -*- Mode: Verilog -*-
// Filename        : SPI_env.sv
// Description     : SPI environment.
// Author          : Adrian Fiergolski
// Created On      : Thu Jan 28 16:46:56 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: SPI_env
//The class configures properly the mother <VIP_env> class.
//~SPI_SS_WIDTH~ - This parameter specifies the number of slaves which can be connected to master on the interface.
class SPI_env #(int SPI_SS_WIDTH = 32) extends VIP_env;
   
   `uvm_component_param_utils_begin( SPI_env#(SPI_SS_WIDTH) )
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <SPI_env> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   // Function: build_phase
   // It configures the instance to use <spi_agent>,
   function void build_phase(uvm_phase phase);
      set_type_override_by_type(mvc_agent::get_type(), spi_agent#(SPI_SS_WIDTH)::get_type() );
      super.build_phase(phase);
   endfunction // build_phase

endclass // SPI_env
