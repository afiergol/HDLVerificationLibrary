//                              -*- Mode: Verilog -*-
// Filename        : SPI_pkg.sv
// Description     : SPI package.
// Author          : Adrian Fiergolski
// Created On      : Thu Jan 28 14:52:40 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifndef SPI_PKG
 `define SPI_PKG

 `include <mvc_macros.svh>
 `include <questa_mvc_svapi.svh>
 `include <mvc_pkg.sv>
 `include <mgc_spi_v1_0_pkg.sv>
 `include "spi_master.sv"
 `include "spi_slave.sv"

package SPI_pkg;

   import uvm_pkg::*;
 `include <uvm_macros.svh>

   //SPI package (Questa VIP)
   import mgc_spi_v1_0_pkg::*;
   import mvc_pkg::*;

   import BasicBlocks_pkg::*;
   
   //Typedef: SPI_config
   typedef VIP_config SPI_config;
   
   //Typedef: SPI_virtual_sqr
   typedef VIP_virtual_sqr SPI_virtual_sqr;

 `include "SPI_env.sv"
   
   export mvc_pkg::mvc_agent;
   export mvc_pkg::mvc_sequencer;
   export mvc_pkg::mvc_item_listener;
   export mvc_pkg::mvc_config_base_id;
   
   export mgc_spi_v1_0_pkg::spi_agent;
   export mgc_spi_v1_0_pkg::spi_vip_config;
   export mgc_spi_v1_0_pkg::spi_master_data_transfer;
   export mgc_spi_v1_0_pkg::spi_top_sequence;
   export mgc_spi_v1_0_pkg::SPI_MSTR;
   export mgc_spi_v1_0_pkg::SPI_SLV;
   export mgc_spi_v1_0_pkg::SPI_MOTO;
   export mgc_spi_v1_0_pkg::SPI_PRECEDE;

   export BasicBlocks_pkg::VIP_monitor_config;

endpackage // SPI_pkg

`endif
