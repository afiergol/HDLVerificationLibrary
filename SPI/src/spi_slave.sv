//                              -*- Mode: Verilog -*-
// Filename        : spi_slave.sv
// Description     : The module connects Mentor's SPI Slave QVIP with DUT master.
//
// Author          : Adrian Fiergolski
// Created On      : Fri May 3 12:49:53 2019
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//The module connects Mentor's SPI Slave QVIP with DUT master.
//Inspired by Mentor's module.
module spi_slave # (
                    int    SPI_SS_WIDTH = 32,
                    string IF_NAME = "null",
                    string PATH_NAME ="uvm_test_top*")
   (
    input 			 sys_clk,
    input 			 reset,
   
    input [(SPI_SS_WIDTH-1) : 0] SS,
    input 			 SCK,
   
    input 			 MOSI,
    input 			 MOSI1,
    input 			 MOSI2,
    input 			 MOSI3,

    output 			 MISO,
    output 			 MISO1,
    output 			 MISO2,
    output 			 MISO3
    );

   import uvm_pkg::*;

   typedef virtual 		 mgc_spi # (SPI_SS_WIDTH) spi_if_t;

   mgc_spi #(SPI_SS_WIDTH) spi_if (sys_clk);

   assign spi_if.MOSI  = MOSI;
   assign spi_if.MOSI1 = MOSI1;
   assign spi_if.MOSI2 = MOSI2;
   assign spi_if.MOSI3 = MOSI3;
   
   assign spi_if.RESET = reset;
   assign MISO  = spi_if.MISO;
   assign MISO1 = spi_if.MISO1;
   assign MISO2 = spi_if.MISO2;
   assign MISO3 = spi_if.MISO3;

   assign spi_if.SS  = SS;
   assign spi_if.SCK = SCK;

   initial
     begin
	uvm_config_db #( spi_if_t )::set(null, PATH_NAME, IF_NAME, spi_if);
     end

endmodule
