//                              -*- Mode: Verilog -*-
// Filename        : genericSimEthernet_virtual_sqr.sv
// Description     : The virtual sequencer of the genericSimEthernet_env.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:27:09 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Variable: genericSimEthernet_virtual_sqr
//The virtual sequencer to run sequences in <genericSimEthernet_env>.
class genericSimEthernet_virtual_sqr extends uvm_virtual_sequencer;

   //Variable: ethernet_sqr
   //Ethernet sequencer
   Ethernet_virtual_sqr ethernet_sqr;

   //Variable: cfg
   //FEC configuration
   genericSimEthernet_config cfg;
   
   `uvm_component_utils_begin( genericSimEthernet_virtual_sqr )
      `uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(ethernet_sqr, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_object_utils_end

   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction

   //Function: build_phase
   function void build_phase(uvm_phase phase);
      uvm_object tmp;
      
      super.build_phase(phase);

      //Set configuration
      if( get_config_object("cfg", tmp, 0) )
	assert( $cast(cfg, tmp) );
      
      if(cfg == null) begin
	 `uvm_fatal("NOCONFIG", "config not set for this component.")
      end
   endfunction
      
endclass // genericSimEthernet_virtual_sqr
