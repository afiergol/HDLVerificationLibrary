//                              -*- Mode: Verilog -*-
// Filename        : genericSimEthernet_env.sv
// Description     : Generic simulation environment with an Ethernet sub-environment.
// Author          : Adrian Fiergolski
// Created On      : Wed Mar 25 17:42:09 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Typedef: genericSimEthernet_scoreboard
//The type of the scoreboard of the genericSimEthernet environment.
typedef scoreboard_main_base genericSimEthernet_scoreboard;

//Class: genericSimEthernet_env
//Generic simulation environment with an Ethernet sub-environment.
class genericSimEthernet_env extends uvm_env;
   
   //Variable: cfg
   //Configuration of the genericSimEthernet_env.
   genericSimEthernet_config cfg;
   
   //Variable: ethernet_env
   //<Ethernet_env>
   Ethernet_env ethernet_env;
   
   //Variable: sqr
   //Virtual sequencer
   genericSimEthernet_virtual_sqr sqr;

   //Variable: scoreboard
   genericSimEthernet_scoreboard scoreboard;
   
   `uvm_component_utils( genericSimEthernet_env )
   
   // Function: new
   //
   // Creates a new <genericSimEthernet_env> with the given instance ~name~ and parent. 
   // If ~name~ is not supplied, the driver is unnamed.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //
   //It creates the internal agents (i.e. Ethernet_env).
   virtual function void build_phase(uvm_phase phase);

      uvm_object tmp;
      
      super.build_phase(phase);

      //Set configuration
      if( get_config_object("genericSimEthernet_cfg", tmp, 0) )
	assert( $cast(cfg, tmp) );
      
      if(cfg == null) begin
	 `uvm_fatal("NOCONFIG", "genericSimEthernet_config not set for this component.")
      end
      
      ////////////////////////
      //Ethernet environment 
      ////////////////////////
      ethernet_env = Ethernet_env::type_id::create("ethernet_env", this);
      set_config_object( "ethernet_env", "cfg", cfg.ethernetCfg, .clone(0));

      ////////////////////
      //Virtual sequencer
      ////////////////////
      sqr = genericSimEthernet_virtual_sqr::type_id::create("sqr", this);
      set_config_object( "sqr", "cfg", cfg, .clone(0) );

      /////////////
      //Scoreboard
      /////////////
      scoreboard = genericSimEthernet_scoreboard::type_id::create("scoreboard", this);
      set_config_object("scoreboard", "cfg", cfg.scoreCfg, .clone(0) );

   endfunction // build_phase

   //Function: connect_phase
   //
   //It connects:
   //- virtual sequencer <sqr> to specific sequencers
   //- non-Ethernet ports to the monitors.
   //- Ethernet submonitors' ports with the <scoreboards>
   virtual function void connect_phase(uvm_phase phase);

      sqr.ethernet_sqr = ethernet_env.sqr;
      
      /////////////////////////////////////
      //Ethernet connection to the scoreboards
      /////////////////////////////////////
      foreach( scoreboard.ports[name] ) begin

	 string kind = name.substr(0, 3);

	 case(kind)
	   "mac_" : begin
	      `uvm_info("connecting MAC analysis port", 
			$psprintf("%s -> %s",
				  ethernet_env.mac_monitor.ap[ name ].get_full_name(),
				  scoreboard.ports[name].get_full_name() ),
			UVM_MEDIUM);
	      ethernet_env.mac_monitor.ap[ name ].connect( scoreboard.ports[name] );
	   end

	   "phy_": begin
	      `uvm_info("connecting PHY analysis port", 
			$psprintf("%s -> %s",
				  ethernet_env.phy_monitor.ap[ name ].get_full_name(),
				  scoreboard.ports[name].get_full_name() ),
			UVM_MEDIUM);
	      ethernet_env.phy_monitor.ap[ name ].connect( scoreboard.ports[name] );
	   end

	   default: begin
	      `uvm_fatal("Connection error", {"Port ", name, " is neither 'mac_' nor 'phy_port'"} );
	   end
	 endcase // case (kind)
	 
      end
   endfunction // connect_phase
   
endclass // genericSimEthernet_env

