//                              -*- Mode: Verilog -*-
// Filename        : genericSimEthernet_config.sv
// Description     : Configuration of the genericSimEthernet_env.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:22:09 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: genericSimEthernet environment configuration.

//Class: genericSimEthernet_seq_config
//
// It holds genericSimEthernet sequence specific configuration.
class genericSimEthernet_seq_config extends uvm_object;

   `uvm_object_utils_begin(genericSimEthernet_seq_config)
   `uvm_object_utils_end

   // Function: new
   //
   // Creates a new <genericSimEthernet_seq_config> with the given instance ~name~.
   function new(string name="genericSimEthernet_seq_config");
      super.new(name);
   endfunction // new
   
endclass // genericSimEthernet_seq_config

//Typedef: genericSimEthernet_scoreboard_config
//The configuration class of the main scoreboard in the genericSimEthernet environment.
typedef scoreboard_main_base_config genericSimEthernet_scoreboard_config;

//Class: genericSimEthernet_config
//
//It holds configuration of the <genericSimEthernet_env>.
class genericSimEthernet_config extends uvm_object;

   //Variable: seqCfg
   //Configure genericSimEthernet's sequences.
   genericSimEthernet_seq_config seqCfg;
   
   //Variable: ethernetCfg
   //Configuration of the Ethernet interface
   Ethernet_config ethernetCfg;

   //Variable: scoreCfg
   //Configuration of the genericSimEthernet's scoreboard
   genericSimEthernet_scoreboard_config scoreCfg;
   
   //Group: Ethernet
   
   //Variable: dut_mac
   //By default it's set to: 00:0a:35:01:E3:21
   protected byte unsigned dut_mac[5:0] = '{'h00, 'h0a, 'h35, 'h01, 'hE3, 'h21};
   //Variable: dut_ip
   //By default it's set to: 10.0.0.2
   protected bit [31:0] dut_ip = {8'h0a, 8'h0, 8'h0, 8'h02};

   //Variable: pc_mac
   //Readout PC MAC address
   protected byte unsigned pc_mac[5:0] = '{ 'he7, 'h53, 'hc9, 'h67, 'h9d, 'hd8};
   
   //Variable: pc_ip
   //IP address of the readout PC
   protected bit [31:0] pc_ip = {8'h0a, 8'h0, 8'h0, 8'h03};

   //Function: set_Ethernet
   //It sets parameters from the Ethernet group.
   //It also copies part required by the <ethernetCfg>
   function void set_Ethernet(const ref byte unsigned dut_mac[5:0],const ref bit [31:0] dut_ip,
			      const ref byte unsigned pc_mac[5:0],const  ref bit [31:0] pc_ip);
      this.dut_mac = dut_mac;
      this.dut_ip = dut_ip;
      this.pc_mac = pc_mac;
      this.pc_ip = pc_ip;
      if(ethernetCfg == null)
	`uvm_fatal("NO_CONFIG", "Instance of the Ethernet configuration hasn't been created");
      
      ethernetCfg.dut_mac = this.dut_mac;
      ethernetCfg.dut_ip = this.dut_ip;
      
   endfunction // set_Ethernet

   //Function: get_Ethernet
   //Function returns parameters from the Ethernet group.
   function void get_Ethernet(ref byte unsigned dut_mac[5:0], ref bit [31:0] dut_ip = this.dut_ip,
			      ref byte unsigned pc_mac[5:0], ref bit [31:0] pc_ip = this.pc_ip);
      dut_mac = this.dut_mac;
      dut_ip = this.dut_ip;
      pc_mac = this.pc_mac;
      pc_ip = this.pc_ip;
   endfunction // get_Ethernet

   //Function: get_Ethernet_dut_mac
   function void get_Ethernet_dut_mac(ref byte unsigned ethernet_mac[5:0]);
      ethernet_mac = this.dut_mac;
   endfunction // get_Ethernet_dut_mac

   //Function: get_Ethernet_dut_ip
   function void get_Ethernet_dut_ip(ref bit [31:0] ethernet_ip);
      ethernet_ip  = this.dut_ip;
   endfunction // get_Ethernet_dut_ip

   //Function: get_Ethernet_pc_mac
   function void get_Ethernet_pc_mac(ref byte unsigned pc_mac[5:0]);
      pc_mac = this.pc_mac;
   endfunction // get_Ethernet_pc_mac

   //Function: get_Ethernet_pc_ip
   function void get_Ethernet_pc_ip(ref bit [31:0] pc_ip);
      pc_ip = this.pc_ip;
   endfunction // get_Ethernet_pc_ip
      
   `uvm_object_utils_begin( genericSimEthernet_config )
      `uvm_field_object(seqCfg, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(ethernetCfg, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(scoreCfg, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_sarray_int(dut_mac, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_int(dut_ip, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_sarray_int(pc_mac, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_int(pc_ip, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_object_utils_end
         
   // Function: new
   //
   // Creates a new <genericSimEthernet_config> with the given instance ~name~.
   function new(string name="genericSimEthernet_config");
      super.new(name);
   endfunction // new

   //Function: createDefault
   //
   //It creates the default configuration classes.
   //~parent~ gives the possibility to profit factory.
   virtual function void createDefault(uvm_component parent = null);
      ethernetCfg = Ethernet_config::type_id::create("Ethernet configuration");
      scoreCfg = genericSimEthernet_scoreboard_config::type_id::create("genericSimEthernet scoreboard configuration");
   endfunction // createDefault
   
endclass // genericSimEthernet_config
