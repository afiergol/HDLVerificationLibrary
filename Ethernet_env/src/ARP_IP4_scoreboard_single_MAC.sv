//                              -*- Mode: Verilog -*-
// Filename        : ARP_IP4_scoreboard_single_MAC.sv
// Description     : The ARP scoreboard checking requests only of the single MAC.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:39:11 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: ARP_IP4_scoreboard_single_MAC

//Class: ARP_IP4_scoreboard_single_MAC_config
//The class to configure the <ARP_IP4_scoreboard_single_MAC>.
class ARP_IP4_scoreboard_single_MAC_config extends scoreboard_base_config;

   //Typedef: MAC_T
   //Derived from <ARP_IP4_scoreboard>.
   typedef ARP_IP4_scoreboard::MAC_T MAC_T;

   //Variable: mac
   //Corresponds to <ARP_IP4_scoreboard_single_MAC::mac>.
   MAC_T mac;
   
   `uvm_object_utils( ARP_IP4_scoreboard_single_MAC_config )

   //Function: new
   //Creates a new <ARP_IP4_scoreboard_single_MAC_config> with the given instance ~name~.
   function new( string name = "ARP_IP4_scoreboard_single_MAC_config" );
      super.new( name );
   endfunction // new

   //Function: set_mac
   //Function sets <mac>
   function void set_mac(byte unsigned mac[5:0]);
      this.mac = MAC_T'(mac);
   endfunction // set_mac

endclass // ARP_IP4_scoreboard_single_MAC_config

//Class: ARP_IP4_scoreboard_single_MAC
//The scoreboard stores requests and replies related to the single specific MA
class ARP_IP4_scoreboard_single_MAC extends ARP_IP4_scoreboard;

   //Variable: mac
   //A specific MAC to be spied.
   MAC_T mac;
   
   `uvm_component_utils_begin(ARP_IP4_scoreboard_single_MAC)
   `uvm_component_utils_end

   //Function: new
   //Constructor
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //It tries to set <mac>.
   function void build_phase(uvm_phase phase);
      ARP_IP4_scoreboard_single_MAC_config cfg_;

      super.build_phase(phase);

      assert( $cast(cfg_, cfg) );
      if(cfg_ == null ) begin
	 `uvm_error("NOCONFIG", "MAC address to be spied hasn't been set");
      end
      else
	mac = cfg_.mac;
   endfunction // build_phase

   //Function: do_receive
   //The function checks first whether target or sender MAC address corresponds to the <mac> and if yes, it calls <ARP_IP4_scoreboard::do_receive>. 
   virtual function void do_receive( string port, eth_1g_arp_ip4_packet frame );
      if( (mac == MAC_T'(frame.source_hardware_addr)) && (mac == MAC_T'(frame.destination_hardware_addr)) )
	super.do_receive(port, frame );
   endfunction // do_receive

endclass // ARP_IP4_scoreboard_single_MAC
