//                              -*- Mode: Verilog -*-
// Filename        : ECHO_scoreboard.sv
// Description     : The scoreboard of the ECHO packets
// Author          : Adrian Fiergolski
// Created On      : Tue Mar 26 13:05:42 2013
// Last Modified By: Adrian Fiergolski
// Last Modified On: Tue Mar 26 13:05:42 2013
// Update Count    : 0
// Status          : Unknown, Use with caution!

//Class: ECHO_scoreboard
//The scoreboard checks ICMP checksums and ECHO requests,replies
class ECHO_scoreboard extends scoreboard_fifos;

   //Group: Statistics

   //Variable: no_frames
   //Number of frames processed by the scoreboard
   int no_frames;

   //Variable: crc_errors
   //Number of CRC errors
   int crc_errors;

   //Variable: echo_requests
   //Number of echo requests
   int echo_requests;

   //Variable: echo_replies
   //Number of echo replies
   int echo_replies;

   //Variable: reply_request_match
   //Number of requests which got a reply
   int reply_request_match;
       
   //Variable: reply_no_request
   //Number of replies which haven't been requested
   int reply_no_request;

   //Variable: requests
   //It contains Echo requests
   //The list of Echo requests indexed by the {Identifier, Sequence Number}
   eth_icmp4_echo_packet requests[int];
   
   `uvm_component_utils( ECHO_scoreboard )

   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction

   //Task: receive
   //The task checks type of the ~frame_~ and calls <do_receive>.
   //<no_frames> counter is increased here.
   task receive( string port, uvm_sequence_item transaction_);

      eth_icmp4_echo_packet frame;
      
      if( ! $cast( frame, transaction_) ) `uvm_error("SCOREBORD", "Wrong ECHO packet received" );
      
      no_frames++;
      do_receive(port, frame);
   endtask // receive

   //Function: do_receive
   //The function calls <echo_reply> or <echo_request> respectively.
   virtual function void do_receive(string port, eth_icmp4_echo_packet frame);
      case( frame.echo_type )
	eth_icmp4_echo_packet::ECHO_REPLY:
	  echo_reply(frame, port);
	eth_icmp4_echo_packet::ECHO_REQUEST:
	  echo_request(frame, port);
      endcase // case ( frame.echo_type )
   endfunction // do_receive
   
   //Function: check_crc
   //Taken from the VIP.
   //It return 1 if CRC is OK, otherwise 0.
   function bit crc_check(input eth_icmp4_echo_packet frame, string port="");
      bit [15:0] check_sum;
      bit [7:0]  t_data[];
      int 	 k,l,m;
      crc_check = 1;

      t_data = new [(4*((frame.total_length/4)-frame.hlen))];
      //converting 32-bit array into 8-bit array
      for(int j=0 ; j<(frame.ip_data.size()) ; j++)
	for(int i=0; i<4; ++i)
	  begin
	     t_data[i + j*4] = frame.ip_data[j][31 -(i*8) -:8];
	  end
      

      // Checksum calculation for IPv4/v6 datagram
      check_sum = eth_ip_checksum::calc_ip_checksum(t_data, (frame.ip_data.size()*4) );
      if(check_sum != 16'b0) begin
	 crc_errors++;
	 crc_check = 0;
	 `uvm_error("ICMP_CRC", {"CRC mismatch on port '", port, "' in the frame: \n", frame.sprint()} )
      end
      else begin
	 `uvm_info( "ICMP_CRC", "CRC match", UVM_MEDIUM );
      end
   endfunction // crc_check


   //Function: echo_reply
   //The function checks CRC and looks for requests.
   virtual function void echo_reply( eth_icmp4_echo_packet frame, string port = "");
      if( crc_check(frame, port) ) begin
	 if(frame.icmp4_data.size < 2) begin
	    `uvm_error("Echo reply", {"Echo reply is too short:\n", frame.sprint()} );
	    return;
	 end
	 
	 echo_replies ++;
	 
	 if( requests.exists( frame.icmp4_data[0] ) ) begin //requests exist
	    eth_icmp4_echo_packet request = requests[ frame.icmp4_data[0] ];

	    if( frame.echo_match( request ) ) begin
	       reply_request_match++;
	       requests.delete( frame.icmp4_data[0] );
	    end
	    else begin
	       `uvm_error("Echo doesn't match", {"Data Echo reply doesn't match to the reply.\nReqeust:\n", 
						 request.sprint(), "Reply:\n", frame.sprint()});
	    end

	 end // if ( requests.exist( frame.icmp4_data[0] ) )
	 else begin

	    reply_no_request++;
	    `uvm_warning("Echo reply", {"The reply was not requested. Reply:\n", frame.sprint()});
	 end
      end
   endfunction // echo_reply

   //Function: echo_request
   //Functions checks CRC and put the frame in the <requests>
   virtual function void echo_request( eth_icmp4_echo_packet frame, string port = "");
      if( crc_check(frame, port) ) begin

	 echo_requests ++;
	 requests[ frame.icmp4_data[0] ] = frame;
      end
   endfunction // echo_request

   //Function: convert2string
   //The function prints statistics of the scoreboard.
   virtual function string convert2string();
      convert2string = $sformatf( {"\n no_frames: %0d",
				   "\n crc_errors: %0d",
				   "\n echo_requests: %0d",
				   "\n echo_replies: %0d",
				   "\n reply_request_match: %0d",
				   "\n reply_no_request: %0d"},
				  no_frames, crc_errors, echo_requests, echo_replies,
				  reply_request_match, reply_no_request);
   endfunction // convert2string

   //Function: report_phase
   //It reports the status.
   function void report_phase(uvm_phase phase);
      int requests_left;

      super.report_phase(phase);

      //Print statistics
      `uvm_info( {get_type_name(), " statistics"} , convert2string(), UVM_LOW );

      requests_left = requests.size();

      if( requests_left == 0 ) begin
	 `uvm_info( {get_type_name(), " requests"}, "All requests got a reply", UVM_LOW);
      end
      else begin
	 `uvm_warning({get_type_name(), " requests"}, $sformatf("There was %d requests which haven't received a reply", requests_left) );
      end
   endfunction // report_phase

   
endclass // ECHO_scoreboard
