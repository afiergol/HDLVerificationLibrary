//                              -*- Mode: Verilog -*-
// Filename        : Eth_mix_seq.sv
// Description     : The sequence sending randomly Ethernet Packets.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:02:22 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: Eth_mix_seq
//The class sends randomly Ethernet Packets.
//Supported packets:
//- eth_1g_device_data_frame data_frame
//- eth_1g_device_control_frame pause_frames
//- eth_arp_ip4_packet
//- eth_icmp4_echo_packet
//- Ping_seq
//- eth_udp_ipv4_packet

class Eth_mix_seq extends Ethernet_seq;

   //Typdef: SEQ_T
   //Type selecting sequence to be generated.
   typedef enum {DATA_FRAME, PAUSE_FRAME_XON, PAUSE_FRAME_XOFF, ARP, ECHO, PING_SEQ, UDP} SEQ_T;

   //Typedef: SEQ_DISTRIBUTION_T
   //Type to describe distribution of probability to generate particular sequence.
   typedef struct{
      int 	 data_frame;
      int 	 pause_frame_xon;
      int 	 pause_frame_xoff;
      int 	 arp;
      int 	 echo; 	 
      int 	 ping_seq;
      int 	 udp;  } SEQ_DISTRIBUTION_T;

   //Variable: seq
   //Sequence to be generated in next run of the loop in <body>
   rand SEQ_T seq;
 
   //Variable: seq_dist
   //Distribution of probability to generate a given sequence.
   SEQ_DISTRIBUTION_T seq_dist = '{10, 1, 1, 10, 10, 10, 10};
   
   //Constraint SEQ_DISTRIBUTION
   constraint SEQ_DISTRIBUTION{
    seq dist { DATA_FRAME         := seq_dist.data_frame,
               PAUSE_FRAME_XON    := seq_dist.pause_frame_xon,
	       PAUSE_FRAME_XOFF   := seq_dist.pause_frame_xoff,
               ARP                := seq_dist.arp,
	       ECHO               := seq_dist.echo,
               PING_SEQ           := seq_dist.ping_seq,
	       UDP                := seq_dist.udp};
   }

   //Variable: dut_ip
   local int 	 dut_ip;

   //Variable: dut_mac
   local byte 	 unsigned dut_mac[5:0];

   //Variable: repeats
   //Defines number of repetitions.
   //Default value is 100.
   int 		 repeats = 100;

   `uvm_object_utils_begin(Eth_mix_seq)
   `uvm_object_utils_end

   //Function: Constructor
   function new(string name="Eth_mix_seq");
      super.new(name);
   endfunction

   extern task body();
   
endclass // Eth_mix_seq

//Task: body
//In the loop repeated <repeats> times, the task randomises <seq> and run it on the sequencer. The sequences don't contain transmission errors.
//At the end after <repeat> packets, provided <seq_dist::pause_frame_xon> != 0,the XON packet is sent in order to avoid transmission stack.
task Eth_mix_seq::body();

   eth_1g_device_data_frame data_frame = eth_1g_device_data_frame::type_id::create("data_frame");
   eth_1g_device_control_frame pause_frame = eth_1g_device_control_frame::type_id::create("pause_frame");
   eth_1g_arp_ip4_packet arp = eth_1g_arp_ip4_packet::type_id::create("arp");
   eth_icmp4_echo_packet echo = eth_icmp4_echo_packet::type_id::create("echo");
   Ping_seq ping_seq = Ping_seq::type_id::create("ping_seq");
   eth_udp_ipv4_packet udp = eth_udp_ipv4_packet::type_id::create("udp");

   dut_ip = p_sequencer.cfg.dut_ip;
   dut_mac = p_sequencer.cfg.dut_mac;

   repeat(repeats) begin

      if( ! this.randomize(seq) ) begin
	`uvm_error("RAND_ERROR", "Randomisation failed");
      end

      case (seq)
	DATA_FRAME: begin
	   start_item(data_frame, .sequencer(p_sequencer.mac_sqr) );
	   if( ! data_frame.randomize() with {tx_error == 0; ext_error == 0;
                                              extension_mode == ETH_1G_EXT_SINGLE_FRAME;}) begin
	      `uvm_error("RAND_ERROR", "Randomisation failed");
	   end
	   
	   finish_item(data_frame);
	end

	PAUSE_FRAME_XON: begin
	   start_item(pause_frame, .sequencer(p_sequencer.mac_sqr) );
	   if( ! pause_frame.randomize() with {tx_error == 0; ext_error == 0;
                                               vlan_mode == ETH_VLAN_UNTAGGED;
                                               extension_mode == ETH_1G_EXT_SINGLE_FRAME;
                                               opcode == 1;

                                               foreach (operand_list[i])  //quanta = 0
	                                         operand_list[i] == 0;
                                               }) begin
	      `uvm_error("RAND_ERROR", "Randomisation failed");
	   end

	   finish_item(pause_frame);
	end

	
	PAUSE_FRAME_XOFF: begin
	   start_item(pause_frame, .sequencer(p_sequencer.mac_sqr) );
	   if( ! pause_frame.randomize() with {tx_error == 0; ext_error == 0;
                                               vlan_mode == ETH_VLAN_UNTAGGED;
                                               extension_mode == ETH_1G_EXT_SINGLE_FRAME;
                                               opcode == 1;

                                               //operand_list[1] is random
                                               foreach (operand_list[i])
	                                          if(i != 1) 
	                                            operand_list[i] == 0;
                                               }) begin
	      `uvm_error("RAND_ERROR", "Randomisation failed");
	   end // if ( ! pause_frame.randomize() with {tx_error == 0; ext_error == 0;...

	   finish_item(pause_frame);
	end

	ARP: begin
	   start_item(arp, .sequencer(p_sequencer.mac_sqr) );
	   if( ! arp.randomize() with {tx_error == 0; ext_error == 0;
                                       extension_mode == ETH_1G_EXT_SINGLE_FRAME;}) begin
	      `uvm_error("RAND_ERROR", "Randomisation failed");
	   end

	   finish_item(arp);
	end

	ECHO: begin
	   start_item(echo, .sequencer(p_sequencer.mac_sqr) );
	   if( ! echo.randomize() with {tx_error == 0; ext_error == 0;
                                         extension_mode == ETH_1G_EXT_SINGLE_FRAME;}) begin
	      `uvm_error("RAND_ERROR", "Randomisation failed");
	   end

	   finish_item(echo);
	end
	
	PING_SEQ: begin
	   if( ! ping_seq.randomize() with {ip_address == '{'h0a000003};}) begin
	      `uvm_error("RAND_ERROR", "Randomisation failed");
	   end
	   ping_seq.start(p_sequencer);
	end

	UDP : begin
	   start_item(udp, .sequencer(p_sequencer.mac_sqr) );
	   if( ! udp.randomize() with 
	       {destination_ip_address == dut_ip;
                foreach( dest_address[i] )
                  dest_address[i] == dut_mac[i];
      
                length < 1300;

                hlen ==5; //open core UDP stack doesn't accept IP header length != 5 bytes
                flag == 0;
                fragmentation_offset == 0;
      
                //MAC layer
                type_mode == ETH_TYPE_0800;
                vlan_mode == ETH_VLAN_UNTAGGED;
		tx_error == 0; ext_error == 0;
                extension_mode == ETH_1G_EXT_SINGLE_FRAME;} ) begin
	      `uvm_error("RAND_ERROR", "Randomisation failed");
	   end // if ( ! udp.randomize() with...

	   finish_item(udp);
	end
	
      endcase // case (seq)

      //the final XON packet
      if(seq_dist.pause_frame_xon) begin
	 start_item(pause_frame, .sequencer(p_sequencer.mac_sqr) );
	 if( ! pause_frame.randomize() with {tx_error == 0; ext_error == 0;
                                             vlan_mode == ETH_VLAN_UNTAGGED;
                                             extension_mode == ETH_1G_EXT_SINGLE_FRAME;
                                             opcode == 1;

                                             foreach (operand_list[i])  //quanta = 0
                                               operand_list[i] == 0;
                                            }) begin
	    `uvm_error("RAND_ERROR", "Randomisation failed");
	 end

	 finish_item(pause_frame);
      end
	
   end // repeat (repeats)

endtask // body
