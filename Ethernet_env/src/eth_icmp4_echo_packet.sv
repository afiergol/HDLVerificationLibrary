//                              -*- Mode: Verilog -*-
// Filename        : eth_icmp4_echo_packet.sv
// Description     : The sequence item for echo request and reply.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:59:33 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: eth_icmp4_echo_packet
//The derived class of the <eth_icmp4_packet> providing IP4 Echo functionality (Ethernet, VLAN not tagged).
class eth_icmp4_echo_packet extends eth_icmp4_packet;

   //Typedef: ECHO_TYPE_T
   //Type of Echo message
   typedef enum {ECHO_REPLY = 'h0, ECHO_REQUEST = 'h8} ECHO_TYPE_T;
   
   //Variable: echo_type
   //Specifies Echo operation (request or reply)
   rand ECHO_TYPE_T echo_type;

   `uvm_object_utils( eth_icmp4_echo_packet )

   //Function: new
   function new(string name="eth_icmp4_echo_packet");
      super.new();
   endfunction // new

   extern virtual function bit do_compare (uvm_object rhs, uvm_comparer comparer);
   extern virtual function void do_copy(uvm_object rhs);
   extern virtual function void do_print(uvm_printer printer);
   extern function void pre_randomize();
   extern task do_receive( mvc_config_base config_base );
   extern function void do_create_received_icmp4_echo_packet();
   extern function bit echo_match(eth_icmp4_echo_packet packet);
     
   //Group: Constraints

   //Constraint: Echo_operation
   constraint Echo_operation
   {
      icmp4_type == echo_type;
   }

   //Constraint: Echo_size
   //Max is 1500 bytes.
   constraint Echo_size
   {
      icmp4_data_length inside { [2:32] };
   }

   //Constraint: Echo_IP4_Flags
   constraint Echo_IP4_Flags
   {
      flag inside { 'h0, 'h2}; 
   }

endclass // eth_icmp4_echo_packet

//Function: pre_randomize
//The function sets:
//- type_mode
//- vlan_mode
//- ver
//- fragmentation_offset
//- protocol
//- icmp4_code
function void eth_icmp4_echo_packet::pre_randomize();

   //Ethernet
   type_mode = ETH_TYPE_0800;
   type_mode.rand_mode(0);
   vlan_mode = ETH_VLAN_UNTAGGED;
   vlan_mode.rand_mode(0);
   
   //IP layer
   ver = 'h4;
   ver.rand_mode(0);
   fragmentation_offset = 'h0;
   fragmentation_offset.rand_mode(0);
   protocol = 'h1; //ICMP
   protocol.rand_mode(0);
   service = 'h0;
   service.rand_mode(0);
   
   //ICMP
   icmp4_code = 'h0;
   icmp4_code.rand_mode(0);

endfunction // pre_randomize

//Function: do_compare
//
//Compares two echo packets.
function bit eth_icmp4_echo_packet::do_compare (uvm_object rhs, uvm_comparer comparer);
   eth_icmp4_echo_packet rhs_;
   do_compare = super.do_compare(rhs,comparer);
   if ( !$cast(rhs_,rhs) ) return 0;

   do_compare &= (echo_type == rhs_.echo_type);
endfunction // do_compare

// Function: do_copy
//
// Copies eth_arp_ip4_packet sequence item
function void eth_icmp4_echo_packet::do_copy (uvm_object rhs);
   eth_icmp4_echo_packet rhs_;
   super.do_copy(rhs);
   if ( ! $cast(rhs_,rhs) ) `uvm_error("ASSERT_FAILURE","Assert statement failure");
   echo_type = rhs_.echo_type;
endfunction // do_copy

// Function: do_print
//
// Prints eth_icmp4_echo_packet sequence item
function void eth_icmp4_echo_packet::do_print (uvm_printer printer);
   printer.print_field("echo_type", echo_type, $bits(echo_type));
   super.do_print(printer);
endfunction

// Task: do_receive
//
// Receives eth_icmp4_echo_packet sequence item
task eth_icmp4_echo_packet::do_receive( mvc_config_base config_base );
   bit valid_frame =1'b0;

   while(valid_frame == 0)
     begin  
	super.do_receive(config_base);
	if( (vlan_mode == ETH_VLAN_UNTAGGED) && 
	    //IP layer
	    (ver == 'h4) && (fragmentation_offset == 'h0) &&
	    (protocol == 'h1) && (service == 'h0) &&
	    //ICMP
	    (icmp4_code == 'h0) && (icmp4_data.size() > 1) &&
	    ( (icmp4_type == ECHO_REQUEST) || (icmp4_type == ECHO_REPLY) ) ) 
            begin
               do_create_received_icmp4_echo_packet();
               valid_frame = 1'b1;
            end 
     end 
endtask

// Function: do_create_received_icmp4_echo_packet
//
// Fetches eth_icmp4_echo_packet sequence item from raw Ethernet packet
function void eth_icmp4_echo_packet::do_create_received_icmp4_echo_packet();
   
   super.do_create_received_icmp4_packet();
   if( ! $cast(echo_type, icmp4_type) ) begin
      `uvm_fatal("CAST_ERROR", "icmp4_type value is out of ECHO_TYPE_T enum type");
   end
endfunction // do_create_received_icmp4_echo_packet

//Function: echo_match
//The function checks whether for the given request/reply the ~packet~ is compatible reply/request.
function bit eth_icmp4_echo_packet::echo_match(eth_icmp4_echo_packet packet);

   echo_match = 0;

   //we have to be request and reply
   case(echo_type)
     ECHO_REPLY:
       
       if( (packet.echo_type != ECHO_REQUEST) ||
	   (dest_address != packet.src_address) )
	 return 0;

     ECHO_REQUEST:
       if( (packet.echo_type != ECHO_REPLY) ||
	   (src_address != packet.dest_address) )
	 return 0;
   endcase // case (echo_type)

   if( icmp4_data.size == packet.icmp4_data.size) begin
      echo_match = 1;
      echo_match &= ( icmp4_data[1][31:16] == packet.icmp4_data[1][31:16] ); //compare first data
      for(int i=2; i < icmp4_data.size; i++)
	echo_match &= icmp4_data[i]== packet.icmp4_data[i];
   end
endfunction // echo_match
