//                              -*- Mode: Verilog -*-
// Filename        : Ethernet_seq.sv
// Description     : Generic Ethernet sequence.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:58:17 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: Ethernet_seq
//Generic sequence executed on <Ethernet_virtual_sqr>.
class Ethernet_seq extends mvc_sequence;

   `uvm_declare_p_sequencer( Ethernet_virtual_sqr )
   `uvm_object_utils_begin( Ethernet_seq )
   `uvm_object_utils_end

   //Function: new
   //Constructor
   function new(string name = "Ethernet_seq" );
      super.new( name );
   endfunction // new

   //Task: pre_body
   //It raises objection.
   task pre_body();
      if(starting_phase != null )
	starting_phase.raise_objection(this, $sformatf("Starting %s", get_name() ) );
   endtask // pre_body
   
   //Task: post_body
   //It drops objection.
   task post_body();
      if(starting_phase != null )
	starting_phase.drop_objection(this, $sformatf("Ending %s", get_name() ) );
   endtask // pre_body

endclass // Ethernet_seq
