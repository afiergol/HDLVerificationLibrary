//                              -*- Mode: Verilog -*-
// Filename        : ARP_seq.sv
// Description     : The sequence spawning ARP sequences.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:40:58 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: ARP_req_seq
//The sequence sends ARP request.
class ARP_req_seq extends Ethernet_seq;

   //Variable: pc_ip4_addr
   rand int pc_ip4_addr;
   //Variable: pc_mac
   rand byte unsigned pc_mac[5:0];
   //Variable: dut_ip4_addr
   rand int dut_ip4_addr;
   
   `uvm_object_utils_begin(ARP_req_seq)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <ARP_req_seq> with the given ~name~.
   function new(string name="ARP_req_seq");
      super.new(name);
   endfunction

   //Task: body
   //The function sends ARP request <eth_1g_arp_ip4_packet>
   task body();
      eth_1g_arp_ip4_packet req = eth_1g_arp_ip4_packet::type_id::create("ARP_request");
      start_item(req, .sequencer(p_sequencer.mac_sqr) );
      if( ! req.randomize() with {opcode == ETH_ARP_REQUEST;
                                  
                                  //MAC address
                                  foreach( source_hardware_addr[i] )
                                     source_hardware_addr[i] == pc_mac[i];
                                  
                                  //ARP
                                  source_ip4_addr == pc_ip4_addr;
                                  destination_ip4_addr == dut_ip4_addr;
                                            
                                  //Ethernet
                                  extension_mode == ETH_1G_EXT_SINGLE_FRAME;
                                  tx_error == 0;
                                  ext_error == 0;}) begin
	 `uvm_error("RAND_ERROR", "Randomisation failed");
      end
      
      finish_item(req);

   endtask // body

endclass // ARP_req_seq
