//                              -*- Mode: Verilog -*-
// Filename        : ARP_IP4_scoreboard.sv
// Description     : Checks whether each ARP request got a proper reply.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:40:09 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: ARP_IP4_scoreboard
//The scoreboard checks whether every ARP request has received a proper reply.
class ARP_IP4_scoreboard extends scoreboard_fifos;

   //Typedef: MAC_T
   //Type to represent a MAC address
   typedef bit[0:47] MAC_T;
   
   //Variable: requests
   //It contains ARP requests waiting for a reply.
   //The first index is a requested IP address. The second index specifies MAC address of the requester.  
   eth_1g_arp_ip4_packet requests[int][MAC_T]; 

   //Group: Statistics
   //Set of variables saving statistics of the scoreboard

   //Variable: arp_frames
   //Number of ARP frames processed by the scoreboard
   int 		     arp_frames;

   //Variable: arp_requests
   //Number of ARP requests processed by the scoreboard
   int 		     arp_requests;
   
   //Variable: arp_replies
   //Number of ARP replies processed by the scoreboard
   int 		     arp_replies;
   
   //Variable: arp_reply_request_match
   //Number of ARP requests which got a reply
   int 		     arp_reply_request_match;
   
   //Variable: arp_reply_no_request
   //Number of ARP replies which haven't been requested
   int 		     arp_reply_no_request;
   
   `uvm_component_utils( ARP_IP4_scoreboard )

   //Function: new
   //Creates a new <ARP_IP4_scoreboard> with the given instance ~name~.
   function new( string name="", uvm_component parent );
      super.new(name, parent);
   endfunction // new

   //Task: receive
   //The task checks type of the received item and calls <do_receive>.
   //The <arp_frames> counter is increased here.
   task receive(string port, uvm_sequence_item transaction_);
      eth_1g_arp_ip4_packet frame;
      
      if( ! $cast( frame, transaction_) ) `uvm_error("SCOREBORD", "Wrong ARP packet received" );
      arp_frames++;

      do_receive(port, frame);
   endtask // receive

   //Function: do_receive
   //The function receives ARP packets from the port in <ports> specified by the ~name~.
   //Afterwards, depending on the kind of the packet, an appropriate function is called.
   virtual function void do_receive(string port, eth_1g_arp_ip4_packet frame);
      
      case( frame.opcode )
	ETH_ARP_REQUEST:
	  arp_request(frame, port);

	ETH_ARP_REPLY:
	  arp_reply(frame, port);

	default: begin
	   `uvm_warning("Unsupported ARP",
			{"The ARP operation is neither request nor reply",
			 "\nReceived from port: ", port,
			 "\nPacket content:\n", frame.sprint() });
	end
      endcase
   endfunction // do_receive
   
   //Function: arp_request
   //Function called, when request has been received.
   virtual function void arp_request(eth_1g_arp_ip4_packet frame, string port="");
      MAC_T mac_addr = MAC_T'(frame.source_hardware_addr);
      int  ip_addr = frame.destination_ip4_addr;
      
      arp_requests++;
      requests[ip_addr][mac_addr] = frame;
   endfunction // receive_request

   //Function: arp_reply
   //Function called when reply has been received.
   virtual function void arp_reply(eth_1g_arp_ip4_packet frame, string port="");
      MAC_T mac_addr = MAC_T'(frame.destination_hardware_addr);
      int  ip_addr = frame.source_ip4_addr;
	   
      arp_replies++;
      if( requests.exists( ip_addr ) ) begin
	 if( requests[ip_addr].exists( mac_addr ) ) begin
	    
	    arp_reply_request_match ++;
	    requests[ip_addr].delete(mac_addr);
	    if( requests[ip_addr].size() == 0)
	      requests.delete( ip_addr);
	 end
	 else begin
	    
	    arp_reply_no_request++;
	    `uvm_warning("ARP: wrong target hardware address", 
			 {"There reply resolves requested IP address, ",
			  "but is not a reply to the request from history",
			  "\nReceived from port: ", port,
			  "\nPacket content:\n", frame.sprint() } );
	 end
      end
      else begin
		 
	 arp_reply_no_request++;
	 `uvm_warning("ARP: no request", 
		      {"The IP address resolved by the reply was not requested",
		       "\nReceived from port: ", port,
		       "\nPacket content:\n", frame.sprint() });
      end
   endfunction // arp_reply

   //Function: convert2string
   //The function prints statistics of the scoreboard.
   virtual function string convert2string();
      convert2string = $sformatf( {"\n arp_frames: %0d",
				   "\n arp_requests: %0d",
				   "\n arp_replies: %0d",
				   "\n arp_reply_request_match: %0d",
				   "\n arp_reply_no_request: %0d"},
				  arp_frames, arp_requests, arp_replies,
				  arp_reply_request_match, arp_reply_no_request);
   endfunction // convert2string
   
   //Function: report_phase
   //It reports the status.
   function void report_phase(uvm_phase phase);
      super.report_phase(phase);

      //Print statistics
      `uvm_info( {get_type_name(), " statistics"} , convert2string(), UVM_LOW );

      if( requests.size() == 0 ) begin
	`uvm_info( {get_type_name(), " requests"}, "All requests got a reply", UVM_LOW);
      end
      else begin
	 int req_no_reply;
	 foreach( requests[ip] )
	   req_no_reply += requests[ip].size();

	 `uvm_warning({get_type_name(), " requests"}, $sformatf("There was %d requests which haven't received a reply", req_no_reply) );
      end
   endfunction // report_phase
      
endclass // ARP_IP4_scoreboard
