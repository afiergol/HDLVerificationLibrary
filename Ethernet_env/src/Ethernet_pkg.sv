//                              -*- Mode: Verilog -*-
// Filename        : Ethernet_pkg.sv
// Description     : Ethernet package.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:56:23 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifndef ETHERNET_PKG_SV
 `define ETHERNET_PKG_SV

`include <mvc_macros.svh>
//The headers below contain packages and modules so can't be included in the Ethernet_pkg
`include <questa_mvc_svapi.svh>
`include <mvc_pkg.sv>
//`include <uvm_container.sv>
`include <mgc_eth_1g_v1_0_pkg.sv>
`include "clk_reset.sv"

`include "BasicBlocks_pkg.sv"

//import interface
import mgc_eth_1g_v1_0_pkg::*;

//Title: Overview
//The Ethernet_pkg provides components required to emulate the Ethernet module. It uses the Ethernet VIP from Mentor Graphics.

//Package: Ethernet_pkg
//
//Package containing Ethernet components

package Ethernet_pkg;

   import uvm_pkg::*;
`include <uvm_macros.svh>

`include "BasicBlocks_pkg.sv"
   import BasicBlocks_pkg::*;
   
   //Interface types
   //Typedef: vEthernet
   //
   //Virtual interface of the Ethernet
   typedef `DEFINE_VIF_TYPE(eth_1g) vEthernet;
      
   //Ethernet 1Gb package (Questa VIP)
   import mvc_pkg::*;
   import mgc_eth_1g_v1_0_pkg::*;
//   import uvm_container_pkg::*;
   import QUESTA_MVC::*;
  
   //maybe useful outside the packet (ie. by the tests)
   export mvc_pkg::mvc_item_listener;
   export mvc_pkg::mvc_sequence;
   export mvc_pkg::mvc_sequence_item_base;
//   export uvm_container_pkg::uvm_container;
   export mgc_eth_1g_v1_0_pkg::eth_udp_ipv4_packet;
   export mgc_eth_1g_v1_0_pkg::eth_1g_device_control_frame;
   export mgc_eth_1g_v1_0_pkg::eth_1g_device_data_frame;
   export mgc_eth_1g_v1_0_pkg::eth_1g_device_ipv4_frame;
   export mgc_eth_1g_v1_0_pkg::ETH_VLAN_UNTAGGED;
   export mgc_eth_1g_v1_0_pkg::ETH_1G_EXT_SINGLE_FRAME;
   export mgc_eth_1g_v1_0_pkg::ETH_TYPE_0800;
   export mgc_eth_1g_v1_0_pkg::ETH_ARP_REQUEST;
   
`include "eth_1g_device_type_frame_constraint_print.sv"
`include "eth_1g_arp_ip4_packet.sv"
`include "eth_icmp4_echo_packet.sv"

`include "uvm2mvc_sequence_item_transformer.sv"
   
`include "ARP_IP4_scoreboard.sv"
`include "ARP_IP4_scoreboard_single_MAC.sv"
`include "ECHO_scoreboard.sv"
`include "ECHO_scoreboard_single_IP.sv"
`include "IP_scoreboard.sv"
`include "UDP_ipv4_scoreboard.sv"
`include "Ethernet_scoreboard.sv"
`include "TCPDump_scoreboard.sv"
  
`include "Ethernet_config.sv"
`include "Ethernet_virtual_sqr.sv"

`include "Ethernet_seq.sv"
`include "Ping_seq.sv"
`include "ARP_table_seq.sv"
`include "ARP_seq.sv"
`include "Eth_mix_seq.sv"
`include "../../Utilities/src/DPIexports.sv"
`include "NetworkBridge_seq.sv"
   
`include "Ethernet_env.sv"

endpackage // Ethernet_pkg

`endif
