//                              -*- Mode: Verilog -*-
// Filename        : Ping_seq.sv
// Description     : Ping sequence.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:01:49 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: Ping_seq
// 
//The sequence PING the FEC card (IPv4).
class Ping_seq extends Ethernet_seq;

   //Variable: mac
   //MAC address of the sequence
   rand byte unsigned mac[5:0];

   //Variable: ip_address
   //IP address of the sequence
   rand int ip_address;

   //Variable: delay
   //Delay between following ARP and ECHO requests.
   realtime delay = 10us;
   
   //Variable: dut_mac
   //The MAC obtained in arp_reply.
   local byte unsigned dut_mac[5:0];

   //Variable: dut_ip
   //The IP address of the DUT (from configuration).
   local int dut_ip;
   
   `uvm_object_utils_begin( Ping_seq )
   `uvm_object_utils_end

   //Function: new
   //Constructor
   function new(string name = "Ping_seq" );
      super.new( name );
   endfunction // new
   
   extern task body();
   extern task send_arp();
   extern task send_echo();
   
   //Group: Constraints
   
   //Constraint: individual_source_address
   constraint individual_source_address
   {
      ( mac[0] % 2 ) == 0;
   }

endclass // Ping_seq

//Task: body
//The task fetches <dut_ip> and calls <send_arp> followed by <send_echo>.
task Ping_seq::body();

   dut_ip  = p_sequencer.cfg.dut_ip;

   send_arp();
   send_echo();

endtask // body

//Task: sent_arp
//The task sends periodically (<delay>) ARP requests until first ARP reply.
task Ping_seq::send_arp();

   eth_1g_arp_ip4_packet arp_req, arp_rep;
   bit done = 0; //indicates whether arp sequence has finished
   
   fork
      //send in loop ARP requests
      begin
	 arp_req = eth_1g_arp_ip4_packet::type_id::create("arp_req");
	 while( ! done)
	   begin
	      `uvm_info("Ethernet ping", "Sending ARP request", UVM_MEDIUM);
	   
	      start_item( arp_req, .sequencer(p_sequencer.mac_sqr) );
	      if( ! arp_req.randomize() with {opcode == ETH_ARP_REQUEST;
                                        
                                                      //MAC address
                                                      foreach( source_hardware_addr[i] )
	                                                source_hardware_addr[i] == mac[i];
                                                      //ARP
                                                      source_ip4_addr == ip_address;
                                                      destination_ip4_addr == dut_ip;

                                                     //Ethernet
                                                     extension_mode == ETH_1G_EXT_SINGLE_FRAME;
	                                             tx_error == 0;
                                                     ext_error == 0;}) begin
		 `uvm_error("RAND_ERROR", "Randomisation failed");
	      end
	      finish_item(arp_req);

	      fork : ARP_timeout
		 //Timeout
		 begin
		    #(delay);
		    `uvm_info("Ethernet ping", "ARP reply TIMEOUT. Resend...", UVM_MEDIUM);
		 end
		 //don't wait if reply has been received
		 wait( done ); 
	      join_any
	      disable ARP_timeout;
	   end // while ( ! done)
      end // fork begin

      //wait for ARP reply
      begin
	 do begin
	    uvm_sequence_item temp;
	    
	    `uvm_info("Ethernet ping", "Waiting for ARP reply", UVM_MEDIUM)
	    p_sequencer.port[ "phy_ARP" ].peek( temp );
	    
	    if( ! $cast(arp_rep, temp) )
	      `uvm_fatal("CAST_ERROR", $sformatf("It's not a %s type", arp_rep.get_type().get_type_name() ) );
	 end
	 while(dut_ip !=  arp_rep.source_ip4_addr && arp_rep.opcode == ETH_ARP_REQUEST);
	 done = 1;
      end
   join

   //copy the replied mac address
   dut_mac =arp_rep.src_address;
   
endtask // send_arp

//Task: send_echo
//The task sends periodically (<delay>) ECHO requests until a first matching ECHO reply.
task Ping_seq::send_echo();

   eth_icmp4_echo_packet echo_req, echo_rep;
   bit done = 0; //indicates whether echo sequence finished
   eth_icmp4_echo_packet reqs[$]; //container to store requests being send so far
   
   fork
      //send in loop ECHO requests
      begin
	 while(! done) begin
	    `uvm_info("Ethernet ping", "Sending Echo request", UVM_MEDIUM);
	    
	    echo_req = eth_icmp4_echo_packet::type_id::create("echo_req");
	    
	    start_item( echo_req, .sequencer( p_sequencer.mac_sqr ) );
	    if( ! echo_req.randomize() with { echo_type == eth_icmp4_echo_packet::ECHO_REQUEST;
                                        
                                                       //IP_address
		                                       source_ip_address == ip_address;
                                                       destination_ip_address == dut_ip;

		                                      //MAC layer
		                                      foreach( src_address[i] )
		                                        src_address[i] == local::mac[i];

		                                      foreach( dest_address[i] )
		                                        dest_address[i] == local::dut_mac[i];

                                                     //Ethernet
		                                     extension_mode == ETH_1G_EXT_SINGLE_FRAME;
		                                     tx_error == 0;
		                                     ext_error == 0;}) begin
	       `uvm_error("RAND_ERROR", "Randomisation failed");
	    end // if ( ! echo_req.randomize() with { echo_type == eth_icmp4_echo_packet::ECHO_REQUEST;...
	    finish_item(echo_req);
	    reqs.push_back(echo_req);
	    
	    fork : ECHO_timeout
	       //TImeout
	       begin
		  #(delay);
		  `uvm_info("Ethernet ping", "Echo reply TIMEOUT. Resend...", UVM_MEDIUM);
	       end
	       //don't wait if reply has been received
	       wait(done);
	    join_any;
	    disable ECHO_timeout;
	 end // while (! done)
      end // fork begin

      //wait for ECHO reply
      begin
	 do begin
	    uvm_sequence_item temp;
	    
	    `uvm_info("Ethernet ping", "Waiting for ECHO reply", UVM_MEDIUM)
	    p_sequencer.port[ "phy_ECHO" ].peek( temp );
		      
	    if( ! $cast(echo_rep, temp) )
	      `uvm_fatal("CAST_ERROR", $sformatf("It's not a %s type", echo_rep.get_type().get_type_name() ) );

	    foreach(reqs[i])
	      if( reqs[i].echo_match(echo_rep) ) begin
		 done = 1;
		 break;
	      end
	 end
	 while( ! done );
      end
   join
endtask // send_echo
