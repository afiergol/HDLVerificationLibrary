//                              -*- Mode: Verilog -*-
// Filename        : uvm2mvc_sequence_item_transformer.sv
// Description     : It converts uvm_sequence_item to mvc_sequence_item_base.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:05:41 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Variable: uvm2mvc_sequence_item_transformer
//It transforms uvm_sequence_item to mvc_sequence_item_base. Useful in case of connection mismatch.
class uvm2mvc_sequence_item_transformer extends uvm_subscriber #(uvm_sequence_item);

   uvm_analysis_port #(mvc_sequence_item_base) port_ap;
   
   `uvm_component_utils_begin(uvm2mvc_sequence_item_transformer)
   `uvm_component_utils_end
   
   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction

   //Function: build_phase
   //It creates <port>.
   function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      port_ap = new("port_ap", this);
   endfunction // build_phase
   
   //Function: write
   //It forward packet to <port> changing type.
   function void write(uvm_sequence_item t);
      mvc_sequence_item_base t_;
      assert( $cast( t_, t) );
      port_ap.write(t_);
   endfunction // write
   
endclass // udp_type_transformer
