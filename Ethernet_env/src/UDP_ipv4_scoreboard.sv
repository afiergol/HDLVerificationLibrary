//                              -*- Mode: Verilog -*-
// Filename        : UDP_ipv4_scoreboard.sv
// Description     : The scoreboard checks cheksum of the UDP packets.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:03:52 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: UDP_ipv4_scoreboard
//The scoreboard checks cheksum of the UDP packets.
//Actually, the class is just a wrapper for the <eth_udp_ipv4_checksum_scoreboard>.
class UDP_ipv4_scoreboard extends scoreboard_base;

   //Variable: udp_checkers
   //Actual class performing checks.
   protected eth_udp_ipv4_checksum_scoreboard udp_checkers[string];

   //Variable: transformers
   //Due to type mismatch.
   protected  uvm2mvc_sequence_item_transformer transformers[string];

   `uvm_component_utils_begin(UDP_ipv4_scoreboard)
   `uvm_component_utils_end
   
   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction

   //Function: build_phase
   //It creates <udp_checker>.
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      foreach( ports[name] ) begin
	 udp_checkers[name] = eth_udp_ipv4_checksum_scoreboard::type_id::create({name, "_udp_checker"}, this);
	 transformers[name] = uvm2mvc_sequence_item_transformer::type_id::create({name, "_transformer"}, this);
      end
   endfunction // build_phase

   //Function: connect_phase
   //It connects the first entry in <ports> with the udp_checker.
   virtual function void connect_phase(uvm_phase phase);
      super.connect_phase(phase);
      foreach( ports[name] ) begin
	 
	 `uvm_info("connecting analysis port", 
		   $sformatf("%s -> %s", ports[name].get_full_name(), 
			     transformers[name].analysis_export.get_full_name() ),
		             UVM_MEDIUM);
	 ports[ name ].connect( transformers[name].analysis_export );

	 `uvm_info("connecting analysis port", 
		   $sformatf("%s -> %s", transformers[name].port_ap.get_full_name(), 
			     udp_checkers[name].m_mac_side_udp_analysis_export.get_full_name() ),
		             UVM_MEDIUM);
	 transformers[name].port_ap.connect( udp_checkers[name].m_mac_side_udp_analysis_export );
      end

   endfunction // connect_phase
   
   
endclass // UDP_ipv4_scoreboard
