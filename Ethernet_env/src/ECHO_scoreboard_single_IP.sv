//                              -*- Mode: Verilog -*-
// Filename        : ECHO_scoreboard_single_IP.sv
// Description     : The ECHO scoreboard tracing only the single IP.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:44:23 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: ECHO_scoreboard_single_IP

//Class: ECHO_scoreboard_single_IP_config
//The class to configure the <ECHO_scoreboard_single_IP>.
class ECHO_scoreboard_single_IP_config extends scoreboard_base_config;

   //Variable: ip
   //Corresponds to the <ECHO_scoreboard_single_IP::ip>.
   int ip;
   
   `uvm_object_utils_begin(ECHO_scoreboard_single_IP_config)
   `uvm_object_utils_end

   //Function: new
   function new(string name="class ECHO_scoreboard_single_IP_config");
      super.new(name);
   endfunction

   //Function: set_ip
   //Function sets <ip>
   function void set_ip(int ip);
      this.ip = ip;
   endfunction // set_ip
endclass // ECHO_scoreboard_single_IP_config
      
//Class: ECHO_scoreboard_single_IP
//The scoreboard traces frames only if either source or destination address match the given ip. 
class ECHO_scoreboard_single_IP extends ECHO_scoreboard;

   //Variable: ip
   //A specific IP to be traced.
   int ip;

   `uvm_component_utils_begin(ECHO_scoreboard_single_IP)
   `uvm_component_utils_end

   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction

   //Function: build_phase
   //It tries to set <ip>.
   function void build_phase(uvm_phase phase);
      ECHO_scoreboard_single_IP_config cfg_;

      super.build_phase(phase);

      assert( $cast(cfg_, cfg) );
      
      if(cfg_ == null ) begin
	 `uvm_error("NOCONFIG", "IP address to be spied hasn't been set");
      end
      else
	ip = cfg_.ip;
   endfunction // build_phase

   //Function: do_receive
   //The function checks whether destination or source IP address corresponds to the <ip> and if yes, it calls <ECHO_scoreboard::do_receive>.
   virtual function void do_receive(string port, eth_icmp4_echo_packet frame);
      if( (ip == frame.source_ip_address) || (ip == frame.destination_ip_address) )
	super.do_receive(port, frame);
   endfunction // do_receive

endclass // ECHO_scoreboard_single_IP
      
      
