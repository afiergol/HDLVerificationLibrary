//                              -*- Mode: Verilog -*-
// Filename        : TCPDump_scoreboard.sv
// Description     : The scoreboard dumps packets into the file.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:02:54 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: TCPDump_scoreboard

//Class: TCPDump_scoreboard_config
//The class to configure the <TCPDump_scoreboard>.
class TCPDump_scoreboard_config extends scoreboard_base_config;

   //Variable: file_name
   //Name of file to which packets will be dump
   string file_name;
   
   `uvm_object_utils_begin(TCPDump_scoreboard_config)
   `uvm_object_utils_end

   //Function: new
   function new(string name="TCPDump_scoreboard_config");
      super.new(name);
   endfunction // new

   //Function: set_file_name
   function void set_file_name(string name);
      this.file_name = name;
   endfunction // set_file_name

endclass // TCPDump_scoreboard_config

import "DPI-C" function chandle OpenCapFile_DPI(string src);
import "DPI-C" function void dumpPacket_DPI(chandle file, byte unsigned packet[], real time_);
import "DPI-C" function void CloseCapFile_DPI(chandle file);

//Class: TCPDump_scoreboard
//The class listens on all ports and save received packets to a file.
//Later the packet can be analysed by TCPDump like software.
//The class requires caplib.
class TCPDump_scoreboard extends scoreboard_fifos;

   //Variable: file_name
   //The file_name to which packets are saved.
   string file_name = "tcp_dump";

   //Variable: file
   //Descriptor to the capfile (type: pcap_dumper_t*)
   local chandle file;

   //Variable: file_semaphore
   //Semaphore to access the file.
   local semaphore file_semaphore;

   `uvm_component_utils_begin(TCPDump_scoreboard)
   `uvm_component_utils_end

   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //The function fetches configuration and tries to open a <file_name>.
   //The function allocates <file_semaphore>.
   function void build_phase(uvm_phase phase);
      TCPDump_scoreboard_config cfg_;

      super.build_phase(phase);

      //fetch configuration
      assert( $cast(cfg_, cfg) );
      
      if(cfg_ == null ) begin
	 `uvm_error("NOCONFIG", {"File name has hasn't been set. Choosing default one: ", file_name });
      end
      else
	file_name = cfg_.file_name;

      //Open a file
      file = OpenCapFile_DPI(file_name);
      if( file == null ) begin
	 `uvm_fatal("FILE_ERROR", "Can't open a file.");
      end

      file_semaphore = new(1);
      
   endfunction // build_phase

   //Task: receive
   //The task receives the packet and relying on port's name calls:
   //- receive_control_frame
   //- receive_data_frame
   //- receive_type_frame
   //At the end dumpPacket is called.
   virtual task receive(string port, uvm_sequence_item transaction_);
      byte unsigned packet[];
      realtime timestamp = $realtime();
      
      case( port.substr(4, port.len() - 1) )   //cut the 'phy_' / 'mac_'
	"CONTROL_frames":
	  receive_control_frame( transaction_, packet );
	"DATA_frames" :
	  receive_data_frame( transaction_, packet );
	"TYPE_frames":
	  receive_type_frame( transaction_, packet );
	default: begin
	  `uvm_warning("Unsupported port", "The scoreboard received packet from the unsupported port");
	end
      endcase // case ( port )

      dumpPacket(packet, timestamp);

   endtask // receive

   //Function: report_phase
   //It closes the file.
   virtual function void report_phase(uvm_phase phase);
      CloseCapFile_DPI(file);
   endfunction // report_phase

   //Task: dumpPacket
   //The method calls dumpPacket_DPI
   task dumpPacket(byte unsigned packet[], realtime timestamp);
      file_semaphore.get(1);
      dumpPacket_DPI(file, packet, timestamp/1us);
      file_semaphore.put(1);
   endtask // dumpPacket
   
   extern task receive_control_frame(uvm_sequence_item transaction_, ref byte unsigned packet[]);
   extern task receive_data_frame(uvm_sequence_item transaction_, ref byte unsigned packet[]);
   extern task receive_type_frame(uvm_sequence_item transaction_, ref byte unsigned packet[]);
endclass // TCPDump_scoreboard

//Task: receive_control_frame
//The task writes to file <eth_1g_device_control_frame>.
task TCPDump_scoreboard::receive_control_frame(uvm_sequence_item transaction_, ref byte unsigned packet[]);
   eth_1g_device_control_frame frame;
   int index = 0;
   int size;

   if( ! $cast( frame, transaction_) ) begin
      `uvm_fatal( "CAST_ERROR", $sformatf("It's not a %s type", frame.get_type().get_type_name() ) );
   end

   size = 60;
   if( frame.vlan_mode == ETH_VLAN_TAGGED )
     size +=2;
   packet = new[ size ];

   foreach( frame.dest_address[i] )
     packet[ index + i ] = frame.dest_address[i];
   index +=6;
   
   foreach( frame.dest_address[i] )
     packet[ index + i ] = frame.src_address[i];
   index +=6;
   
   if( frame.vlan_mode == ETH_VLAN_TAGGED) begin
      packet[ index++ ] =  frame.vlan_tag[15:8];
      packet[ index++ ] =  frame.vlan_tag[7:0];
   end
   packet[ index++ ] = 'h88;
   packet[ index++ ] = 'h08;
   packet[ index++ ] = frame.opcode[15:8];
   packet[ index++ ] = frame.opcode[7:0];
   foreach( frame.operand_list[i] )
     packet[ index + i ] = frame.operand_list[i];
   index +=44;
endtask // receive_control_frame

//Task: receive_data_frame
//The task writes to file <eth_1g_device_data_frame>
task TCPDump_scoreboard::receive_data_frame(uvm_sequence_item transaction_, ref byte unsigned packet[]);
   
   eth_1g_device_data_frame frame;
   int index = 0;
   int size;
   
   if( ! $cast( frame, transaction_) ) begin
      `uvm_fatal( "CAST_ERROR", $sformatf("It's not a %s type", frame.get_type().get_type_name() ) );
   end

   size = frame.mac_client_data.size() + 14;
   if( frame.vlan_mode == ETH_VLAN_TAGGED )
     size +=2;
   packet = new[ size ];
   
   foreach( frame.dest_address[i] )
     packet[ index + i ] = frame.dest_address[i];
   index +=6;
   
   foreach( frame.dest_address[i] )
     packet[ index + i ] = frame.src_address[i];
   index +=6;
   
   if( frame.vlan_mode == ETH_VLAN_TAGGED) begin
      packet[ index++ ] =  frame.vlan_tag[15:8];
      packet[ index++ ] =  frame.vlan_tag[7:0];
   end

   packet[ index++ ] = frame.len_type_field[15:8];
   packet[ index++ ] = frame.len_type_field[7:0];

   foreach( frame.mac_client_data[i] )
     packet[ index + i ] = frame.mac_client_data[i];

endtask // receive_date_frame

//Task: receive_type_frame
//The task writes to file <eth_1g_device_type_frame>
task TCPDump_scoreboard::receive_type_frame(uvm_sequence_item transaction_, ref byte unsigned packet[]);
   
   eth_1g_device_type_frame frame;
   int index = 0;
   int size;
   
   if( ! $cast( frame, transaction_) ) begin
      `uvm_fatal( "CAST_ERROR", $sformatf("It's not a %s type", frame.get_type().get_type_name() ) );
   end

   size = frame.mac_client_data.size() + 14;
   if( frame.vlan_mode == ETH_VLAN_TAGGED )
     size +=2;
   packet = new[ size ];
   
   foreach( frame.dest_address[i] )
     packet[ index + i ] = frame.dest_address[i];
   index +=6;
   
   foreach( frame.dest_address[i] )
     packet[ index + i ] = frame.src_address[i];
   index +=6;
   
   if( frame.vlan_mode == ETH_VLAN_TAGGED) begin
      packet[ index++ ] =  frame.vlan_tag[15:8];
      packet[ index++ ] =  frame.vlan_tag[7:0];
   end

   if( frame.type_mode == ETH_TYPE_CUSTOM_PACKET) begin
      packet[ index++ ] = frame.type_value[15:8];
      packet[ index++ ] = frame.type_value[7:0];
   end
   else begin
      packet[ index++ ] = frame.type_mode[15:8];
      packet[ index++ ] = frame.type_mode[7:0];
   end
   
   foreach( frame.mac_client_data[i] )
     packet[ index + i ] = frame.mac_client_data[i];

endtask // receive_type_frame

   

