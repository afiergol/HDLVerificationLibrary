//                              -*- Mode: Verilog -*-
// Filename        : ARP_table_seq.sv
// Description     : The ARP table of packets being sent by the MAC.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:42:26 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class ARP_table_seq

//The sequence peeks all packets being sent by the MAC and builds ARP table of their MAC and IP addresses. It also looks for ARP requests from the PHY and if needed, sends ARP reply.
class ARP_table_seq extends Ethernet_seq;

   //Variable: macs
   //ARP table of the sequence  
   byte unsigned macs[int][5:0];

   //Variable: reply_prob
   //The probability that <ARP_tabe_seq> will reply.
   //Useful to inject errors.
   int 	unsigned reply_prob =90;

   `uvm_declare_p_sequencer( Ethernet_virtual_sqr)
   `uvm_object_utils_begin(ARP_table_seq)
   `uvm_object_utils_end

   //Function: new
   function new(string name="ARP_table_seq");
      super.new(name);
   endfunction // new

   extern virtual task body();
   extern task peek_mac();
   extern task serve_ethernet();
   
endclass // ARP_table_seq

//Task: body
//The task spawn in parallel two task:
//-<peek_mac>
//-<serve_ethernet>
task ARP_table_seq::body();
   `uvm_info( "ARP_table", "Starting ARP table", UVM_MEDIUM);
   fork
      peek_mac();
      serve_ethernet();
   join
endtask // body

//Task: peek_mac
//The task peeks packets from Ethernet and build <macs>.
task ARP_table_seq::peek_mac();
   forever begin
      uvm_sequence_item temp;
      eth_ip_packet ip_packet;
      typedef bit [47:0] MAC_PRINT_T;
      MAC_PRINT_T mac_print;
      
      p_sequencer.port[ "mac_IP" ].peek( temp );

      if( ! $cast(ip_packet, temp) )
	`uvm_fatal("CAST_ERROR", $sformatf("It's not a %s type", ip_packet.get_type().get_type_name() ) );

      foreach( ip_packet.src_address[i] )
	macs[ ip_packet.source_ip_address ][i] = ip_packet.src_address[i];

      mac_print = MAC_PRINT_T'( ip_packet.src_address );
      `uvm_info( "New ARP entry", $sformatf("The IP %8x has mac address %12x", 
					    ip_packet.source_ip_address, mac_print), UVM_HIGH);
   end
   
endtask // peek_mac

//Task: serve_ethernet
//The task waits for ARP requests from PHY. Afterwards with the probability of <reply_prob>.
//The priority of spawned ARP replies is 5.
//The ARP replies don't contain tx_errors and ext_errors.
task ARP_table_seq::serve_ethernet();
   
   eth_1g_arp_ip4_packet arp_rep;
   arp_rep = eth_1g_arp_ip4_packet::type_id::create("arp_rep");
   
   forever begin
      uvm_sequence_item temp;
      eth_1g_arp_ip4_packet arp_req;

      p_sequencer.port[ "phy_ARP" ].peek( temp );

      if( ! $cast(arp_req, temp) )
	`uvm_fatal("CAST_ERROR", $sformatf("It's not a %s type", arp_req.get_type().get_type_name() ) );
      
      if(arp_req.opcode == ETH_ARP_REQUEST) begin
	 
	 `uvm_info("ARP REQUEST", $sformatf( "PHY requested MAC of the IP: %8x", arp_req.destination_ip4_addr), UVM_MEDIUM);
	 
	 if( macs.exists( arp_req.destination_ip4_addr ) ) begin
	    
	    if( $urandom_range(100,1) < reply_prob ) begin
	       int ip_addr = arp_req.destination_ip4_addr;

	       start_item( arp_rep, , p_sequencer.mac_sqr);
	       if( ! arp_rep.randomize() with {opcode == ETH_ARP_REPLY;
                                               
                                               //MAC address
                                               foreach( source_hardware_addr[i] )
						  source_hardware_addr[i] == macs[ip_addr][i];
					       foreach( destination_hardware_addr[i] )
					       destination_hardware_addr[i] == arp_req.source_hardware_addr[i];
					       
                                               //ARP
                                               source_ip4_addr == ip_addr;
                                               destination_ip4_addr == arp_req.source_ip4_addr;
					       
                                               //Ethernet
                                               extension_mode == ETH_1G_EXT_SINGLE_FRAME;
	                                       tx_error == 0;
                                               ext_error == 0;}) begin
		  `uvm_error("RAND_ERROR", "Randomisation failed");
	       end // if ( ! arp_rep.randomize() with {opcode == ETH_ARP_REPLY;...

	       `uvm_info("ARP REPLY", "Sending ARP reply on PHY's request", UVM_MEDIUM );
	       finish_item(arp_rep);
	    end // if ( $urandom_range(100,1) < reply_prob )
	 end // if ( macs.exists( arp_req.destination_ip4_addr ) )
	 else begin
	    `uvm_warning("Unknown IP", "PHY requested unknown IP address");
	 end // else: !if( macs.exists( arp_req.destination_ip4_addr ) )

      end // if (arp_req.opcode == ETH_ARP_REQUEST)
   end
endtask // serve_ethernet
