//                              -*- Mode: Verilog -*-
// Filename        : eth_1g_arp_ip4_packet.sv
// Description     : The ARP IP4 packet.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:48:20 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: eth_1g_arp_ip4_packet
//
//The class provides IP4 ARP (Ethernet, VLAN not tagged).
class eth_1g_arp_ip4_packet extends eth_1g_device_arp_rarp_frame;

   //Variable: source_ip4_addr
   //The IP4 sender addr
   rand int source_ip4_addr;

   //Variable: source_ip4_addr
   //The IP4 sender addr
   rand int destination_ip4_addr;
   
   `uvm_object_utils_begin(eth_1g_arp_ip4_packet)
   `uvm_object_utils_end

   //Function: new
   function new(string name="eth_1g_arp_ip4_packet");
      super.new(name);
   endfunction // new

   extern virtual function bit do_compare (uvm_object rhs, uvm_comparer comparer);
   extern virtual function void do_copy(uvm_object rhs);
   extern virtual function void do_print(uvm_printer printer);
   extern function void pre_randomize();
   extern function void post_randomize();
   extern task do_receive( mvc_config_base config_base );

endclass // eth_1g_arp_ip4_packet

//Function: pre_randomize
//Functions sets:
//- protocol_type
//- vlan_mode
//Later it calls <eth_1g_arp_packet::pre_randomize>
function void eth_1g_arp_ip4_packet::pre_randomize();

   vlan_mode = ETH_VLAN_UNTAGGED;
   vlan_mode.rand_mode(0);

   protocol_type = ETH_TYPE_0800;
   protocol_type.rand_mode(0);

   source_protocol_addr = new[4];
   source_protocol_addr.rand_mode(0);

   destination_protocol_addr = new[4];
   destination_protocol_addr.rand_mode(0);
   
   super.pre_randomize();

endfunction // pre_randomize

//Function: post_randomize
//Function copies <destination_ip4_addr> and <source_ip4_addr>.
function void eth_1g_arp_ip4_packet::post_randomize();
   foreach ( source_protocol_addr[i] )
     source_protocol_addr[i] = source_ip4_addr[31-8*i -:8];

   foreach ( source_protocol_addr[i] )
     destination_protocol_addr[i] = destination_ip4_addr[31-8*i -:8];

   super.post_randomize();

endfunction // post_randomize

//Function: do_compare
//
//Compares two eth_1g_arp_ip4_packet sequence items.
function bit eth_1g_arp_ip4_packet::do_compare (uvm_object rhs, uvm_comparer comparer);
   eth_1g_arp_ip4_packet rhs_;
   do_compare = super.do_compare(rhs,comparer);
   if ( !$cast(rhs_,rhs) ) return 0;

   do_compare &= (source_ip4_addr == rhs_.source_ip4_addr);
   do_compare &= (destination_ip4_addr == rhs_.destination_ip4_addr);
endfunction // do_compare

// Function: do_copy
//
// Copies eth_1g_arp_ip4_packet sequence item
function void eth_1g_arp_ip4_packet::do_copy (uvm_object rhs);
   eth_1g_arp_ip4_packet rhs_;
   super.do_copy(rhs);
   if ( ! $cast(rhs_,rhs) ) `uvm_error("ASSERT_FAILURE","Assert statement failure");
   source_ip4_addr = rhs_.source_ip4_addr;
   destination_ip4_addr = rhs_.destination_ip4_addr;
endfunction // do_copy

// Function: do_print
//
// Prints eth_1g_arp_ip4_packet sequence item

function void eth_1g_arp_ip4_packet::do_print (uvm_printer printer);
   printer.print_field("source_ip4_addr", source_ip4_addr, $bits(source_ip4_addr));
   printer.print_field("destination_ip4_addr", destination_ip4_addr, $bits(destination_ip4_addr));

   super.do_print(printer);

endfunction

// Task: do_receive
//
// Receives eth_1g_arp__ip4_packet sequence item

task eth_1g_arp_ip4_packet::do_receive( mvc_config_base config_base );
   bit valid_frame =1'b0;

   while(valid_frame == 0)
     begin  
	super.do_receive(config_base);
	if( ( hardware_type == 'h01 ) && (protocol_type = ETH_TYPE_0800) &&
	    ( vlan_mode == ETH_VLAN_UNTAGGED ) && ( protocol_addr_length == 4 ) &&
	    ( hardware_addr_length == 6) )
          begin
	     source_ip4_addr = int'(source_protocol_addr);
	     destination_ip4_addr = int'(destination_protocol_addr);
             valid_frame = 1'b1;
          end 
     end 
endtask
