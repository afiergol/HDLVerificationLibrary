// Filename        : capFile.c
// Description     : DPI functions calling PCAP library to dump Ethernet frames.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:00:02 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

#include "pcap.h"
#include "svdpi.h"

void* OpenCapFile_DPI(const char* file)
{
  pcap_t *pcap = pcap_open_dead( DLT_EN10MB, 65635 );
  return (void*) pcap_dump_open( pcap, file);
}


void dumpPacket_DPI(const void* file, const svOpenArrayHandle packet, const double time_)
{
  struct pcap_pkthdr packet_header;
  pcap_t* const pcap = (pcap_t*) file;

  //Fill header
  packet_header.len = (bpf_u_int32) svSizeOfArray(packet);
  packet_header.caplen = packet_header.len;
  packet_header.ts.tv_sec = (long int) time_ / 1000000;
  packet_header.ts.tv_usec = (long int) time_ % 1000000;

  //Dump the packet
  pcap_dump( (u_char *) pcap, &packet_header, (const u_char *) svGetArrayPtr( packet ) );
  pcap_dump_flush( (pcap_dumper_t *) file);
}

void CloseCapFile_DPI(const void* file)
{
  pcap_dump_close( (pcap_dumper_t *) file );
}
