// Filename        : networkBridge.cpp
// Description     : DPI functions briding at UDP level the SV simulation with the network.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:00:02 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

#include <iostream>
#include <cmath>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/atomic.hpp>
#include <boost/shared_ptr.hpp>
#include <svdpi.h>
#include <unistd.h>

extern "C"
{

#include "../../../Utilities/src/DPI/DPIUtilities.hpp"

  using boost::asio::ip::udp;
  
  //task in SV which enables progress in simulation time
  extern int svSleep(long long);

  class UDP_server{

  public:
    UDP_server( boost::asio::io_service & io_service, udp::endpoint local_endpoint, const unsigned int BUFFER_SIZE, const long long SLEEP_TIME) : 
      socket_(io_service, local_endpoint), recv_buffer(BUFFER_SIZE), io_service_(& io_service), SLEEP_TIME(SLEEP_TIME)
    {
      bytes_received_ = 0;
      start_receive();
      thread_ = boost::thread( boost::bind( &UDP_server::listening_thread, boost::ref(*this) ) );
    }
    
    ~UDP_server()
    {
      io_service_->stop();  //stop IO service
      thread_.join();       //wait the listening thread to finish
    }
    
    int receivePacket(svOpenArrayHandle buffer, unsigned int * bytes_received, 
		      const char** remote_address, unsigned short int * remote_port)
    {
      unsigned char *svBuffer = (unsigned char*) svGetArrayPtr(buffer);
      while( ! bytes_received_ ) {
	if( svSleep(SLEEP_TIME) ) //was the task svSleep disabled ?
	  throw taskDisabled();
      }

      //new packet has arrived
      {//scope of the lock
	//lock mutex so server thread will no pass buffer to socket waiting for next packet
	boost::lock_guard< boost::mutex > lock (mutex_);
	
	//isn't received packet to big to be passed to SV ? 
	if( bytes_received_ > svSizeOfArray(buffer) ) {
	  std::cout << "NetworkBridge DPI error: the received packet is bigger than the available SV buffer. Packet is skipped" << std::endl;
	}
	else{  //size of the new packet is ok

	  //clarify remote endpoint
	  remote_address_ = remote_endpoint_.address().to_string();
	  *remote_address = remote_address_.c_str();
	  *remote_port = remote_endpoint_.port();
	  
	  //copy packet to SV
	  *bytes_received = bytes_received_;
	  int i;
	  std::vector<unsigned char>::iterator it;
	  for( i = 0, it = recv_buffer.begin() ; i< bytes_received_; ++i, ++it)
	    svBuffer[i] = *it;
	}
	
	bytes_received_ = 0;                //wait for next data
      }
      received_packet_served_.notify_one(); //resume the listening thread
      
      if( svIsDisabledState() )   //in case the SV task has been disabled in the meantime
	throw taskDisabled();

      return 0;
    }

    int sendPacket ( const void* bridge, const svOpenArrayHandle buffer, 
		     const char* remote_address, const unsigned short int remote_port)
    {
      boost::system::error_code error;
      boost::asio::ip::address remote_address_;
      remote_address_.from_string(remote_address);
      udp::endpoint remote_endpoint(remote_address_, remote_port);
      unsigned char * svBuffer = (unsigned char*) svGetArrayPtr(buffer);
      socket_.send_to(boost::asio::buffer( svBuffer, svSizeOfArray(buffer) ), remote_endpoint, 0, error);
      if( error.value() )
	std::cout <<  "Network Bridge DPI error: " << error.message() << error << std::endl;
      return error.value();
    }

  private :
    void start_receive()
    {
      socket_.async_receive_from(boost::asio::buffer( recv_buffer ), remote_endpoint_,
				 boost::bind( &UDP_server::handle_receive, this, 
					      boost::asio::placeholders::error,
					      boost::asio::placeholders::bytes_transferred) );
    }
    
    void handle_receive(const boost::system::error_code& error, std::size_t bytes_received)
    {
      if( !error )
	{
	  //std::cout<<"Received packet size " << bytes_received <<std::endl;
	  bytes_received_ = bytes_received;
	}
      else
	std::cout<< "Network Bridge DPI error: " << error.message() <<  error << std::endl;
      
      //wait for another thread to copy data from the buffer to SV
      boost::unique_lock<boost::mutex> lock(mutex_);
      while(! bytes_received_ )
	received_packet_served_.wait(lock);
      //start recepetion of next packet
      start_receive();
    }
    
    void listening_thread()
    {
      std::cout << "Network Bridge DPI: Starting UDP listening thread" <<std::endl;
      io_service_->run();
      std::cout << "Network Bridge DPI: Finishing UDP listening thread" <<std::endl;
    }

    udp::socket socket_;
    udp::endpoint remote_endpoint_;
    std::string remote_address_;   //as const char ** is returned to SV, the class needs to keep a string which 
                                   //exists even after forwardNet2Sim_DPI call
    std::vector<unsigned char> recv_buffer;
    mutable boost::atomic<int> bytes_received_;
    boost::shared_ptr< boost::asio::io_service > io_service_;
    boost::mutex mutex_;
    boost::condition_variable received_packet_served_;  //the received packed has been coppied to SV and server may start listen 
    boost::thread thread_;
    const long long SLEEP_TIME;
  };

  void* initNetworkBridge_DPI(char* pc_ip_, short int pc_port, const unsigned int BUFFER_SIZE, 
			      const long long SLEEP_TIME) {

    try
      {
	//address
	boost::asio::ip::address pc_ip;
	pc_ip.from_string(pc_ip_);
	udp::endpoint local_endpoint(udp::v4(), pc_port);
	local_endpoint.address(pc_ip);
	
	//UDP server
        UDP_server *server = new UDP_server( *(new boost::asio::io_service), local_endpoint, BUFFER_SIZE, SLEEP_TIME);
	return (void* ) server;
      }
    catch (std::exception& e){
      std::cout << "NetworkBridge DPI error: " << e.what() << std::endl;
      return NULL;
    }
  }

  int forwardNet2Sim_DPI(const void* bridge, const svOpenArrayHandle buffer, unsigned int * bytes_received,
			 const char** remote_address, unsigned short int * remote_port){
    UDP_server * const server = (UDP_server*) bridge;
    try
      {
	return server->receivePacket(buffer, bytes_received, remote_address, remote_port);
      }
    catch ( taskDisabled & e)
      {
	delete server;
	svAckDisabledState();
	return 1;
      }
  }
  
  int forwardSim2Net_DPI( const void* bridge, const svOpenArrayHandle buffer, 
			   const char* remote_address, const unsigned short int remote_port){
    UDP_server * const server = (UDP_server*) bridge;
    return server->sendPacket(bridge, buffer, remote_address, remote_port);
  }
}
