//                              -*- Mode: Verilog -*-
// Filename        : NetworkBridge_seq.sv
// Description     : The sequence bridges the simulation with the network.
// Author          : Adrian Fiergolski
// Created On      : Fri Mar 27 11:39:26 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.


//Class: NetworkBridge_seq
//The sequence bridges the simulation via DPI with a real network interface.
class NetworkBridge_seq extends Ethernet_seq;
   
   `uvm_object_utils_begin(NetworkBridge_seq)
   `uvm_object_utils_end

   //Variable: bridge
   //Chandler to a DPI bridge.
   local chandle bridge;

   //Group: Configuration

   //Variable: dut_mac
   //uASIC MAC address
   byte unsigned dut_mac[5:0] = '{ 'h21, 'hE3, 'h01, 'h35, 'h0a, 'h00 };

   //Variable: dut_ip
   //IP address of the uASIC
   bit [31:0] dut_ip =  {8'h0a, 8'h0, 8'h0, 8'h02};

   //Variable: pc_mac
   //Readout PC MAC address
   byte  unsigned pc_mac[5:0] = '{ 'he7, 'h53, 'hc9, 'h67, 'h9d, 'hd8};
   
   //Variable: pc_ip
   //IP address of the readout PC
   bit [31:0] pc_ip = {8'h0a, 8'h0, 8'h0, 8'h03};
   
   //Variable: pc_port
   //The bridge listens on the given udp port in the network.
   int unsigned pc_port;

   //Parameter: UDP_SIZE
   //The maximum UDP packer size
   parameter int unsigned UDP_SIZE=2**16;

   //Variable: WAIT_PERIOD
   //Defines period in which the bridge will be checked for new packets.
   parameter time WAIT_PERIOD = 100ns;
   
   //Function: new
   //Creates a new <NetworkBridge_seq> with the given ~name~.
   function new(string name="NetworkBridge_seq");
      super.new(name);
   endfunction // new

   //Task: pre_body
   //It calls <initNetworkBridge>
   task pre_body();
      super.pre_body();
      initNetworkBridge();
   endtask // pre_body

   //Task: body
   //The task spawns in parallel:
   //-<forwardSim2Net>
   //-<forwardNet2Sim>
   virtual task body();
      fork
	 forwardSim2Net();
	 forwardNet2Sim();
      join
   endtask // body

   //Function: initNetworkBridge
   //The functions initialises network bridge on DPI side.
   extern function void initNetworkBridge();

   //Task: forwardNet2Sim
   //The task forwards udp packages from network to the simulation.
   extern task forwardNet2Sim();

   //Task: forwardSim2Net
   //The task forwards udp packages from simulation to network.
   extern task forwardSim2Net();

   //Task: peek_mac
   //The task waits for UDP packets from PHY.
   extern task peek_mac(ref eth_udp_ipv4_packet udp);

endclass // NetworkBridge_seq

import "DPI-C" function chandle initNetworkBridge_DPI(string pc_ip, shortint pc_port, int unsigned BUFFER_SIZE, time SLEEP_TIME );
import "DPI-C" context task forwardNet2Sim_DPI(chandle bridge, output byte unsigned buffer[], output int unsigned bytes_received,
					       output string remote_address, output shortint unsigned remote_port );
import "DPI-C" function int forwardSim2Net_DPI(chandle bridge, byte unsigned buffer[], string remote_address, shortint unsigned remote_port);
   
   function void NetworkBridge_seq::initNetworkBridge();
      string 						     pc_ip_ = $sformatf("%0d", pc_ip[ $bits(pc_ip)-1 -:8]);
      for(int unsigned i = 1; i < $bits(pc_ip)/8; i++)
	pc_ip_ = $sformatf( "%s.%0d", pc_ip_, pc_ip[ $bits(pc_ip)-1 - 8*i -:8]);

      `uvm_info( "Network Bridge", $sformatf("Initialising network bridge - IP: %s, UDP port: %0d ", pc_ip_, pc_port), UVM_MEDIUM);
      bridge = initNetworkBridge_DPI(pc_ip_, pc_port, UDP_SIZE, WAIT_PERIOD);
      if( bridge == null)
	`uvm_error( "Network Bridge", "Network initialisation failed");
   endfunction // initNetworkBridge
   
   task NetworkBridge_seq::forwardNet2Sim();
      byte unsigned buffer[UDP_SIZE];
      int  unsigned bytes_received;
      shortint unsigned remote_port;
      string   remote_address;
      
      eth_udp_ipv4_packet udp = eth_udp_ipv4_packet::type_id::create("upd_received");
      
      forever begin
	 forwardNet2Sim_DPI(bridge, buffer, bytes_received, remote_address, remote_port);
	 
	 if(!bytes_received) begin
	    `uvm_error( "Network Bridge", "forwardNet2Sim_DPI failed");
	 end
	 else begin
	    logic [31:0] remote_ip;  //IP written in 32 bits

	    if( $sscanf( remote_address, "%0d.%0d.%0d.%0d", remote_ip[31:24], remote_ip[23:16], remote_ip[15:8], remote_ip[7:0] ) != 4) begin
	       `uvm_error ("Network Bridge", "Wrong format of the received IP address string");
	    end
	    else begin
	       `uvm_info( "Network Bridge", $sformatf("Received a new UDP packet (size: %0d B)", bytes_received), UVM_HIGH);
	       
	       start_item(udp, .sequencer(p_sequencer.mac_sqr) );
	       if( ! udp. randomize () with {  //MAC layer
		  foreach (src_address[i])
	             src_address[i] == pc_mac[i];
		  foreach (dest_address[i])
	             dest_address[i] == dut_mac[i];

		  //IP layer
		  hlen == 5;
		  source_ip_address == remote_ip;
		  destination_ip_address == dut_ip;
		  service == 'h0;
		  flag == 'h2;  //Don't fragment
		  fragmentation_offset == 0;

		  //UDP layer
		  destination_port_address == pc_port;
		  source_port_address == remote_port;

		  foreach( udp_data [i] )
		     udp_data[i] == {buffer[4*i], buffer[4*i+1], buffer[4*i+2], buffer[4*i+3]};
		  length == bytes_received + 8; //+UDP header
		  
		  //errors
		  type_mode == ETH_TYPE_0800;
                  vlan_mode == ETH_VLAN_UNTAGGED;
		  extension_mode == ETH_1G_EXT_SINGLE_FRAME;
		  tx_error == 0;
		  tx_error_on_cycle == 0;
		  ext_error == 0; }) begin
		  `uvm_error("RAND_ERROR", "Randomisation failed");
	       end // if ( ! upd. randomize () with {...
	       finish_item(udp);
	    end // else: !if( $sscanf( remote_address, "%0d.%0d.%0d.%0d", remote_ip[31:24], remote_ip[23:16], remote_ip[15:8], remote_ip[7:0] ) )
	 end // else: !if(!bytes_received)
      end // forever begin
   endtask // forwardNet2Sim

   task NetworkBridge_seq::forwardSim2Net();
      eth_udp_ipv4_packet udp = eth_udp_ipv4_packet::type_id::create("upd_send");
      byte unsigned buffer[];
      string remote_address;
      forever begin
	 peek_mac(udp);
	 buffer = new [udp.length];
	 foreach(buffer[i])
	   buffer[i] = udp.udp_data[i/4][31-(i%4)*8 -:8];
	 remote_address = $sformatf( "%0d.%0d.%0d.%0d", udp.destination_ip_address[31:24], udp.destination_ip_address[23:16],
				     udp.destination_ip_address[15:8], udp.destination_ip_address[7:0]);
	 if( forwardSim2Net_DPI(bridge, buffer, remote_address, udp.destination_port_address) )
	   `uvm_error( "Network Bridge", "forwardSim2Net_DPI failed");
      end
   endtask // forwardSim2Net

   task NetworkBridge_seq::peek_mac(ref eth_udp_ipv4_packet udp);
      uvm_sequence_item temp;
      p_sequencer.port[ "phy_UDP" ].peek( temp );

      if( ! $cast(udp, temp) )
	`uvm_fatal("CAST_ERROR", $sformatf("It's not a %s type", udp.get_type().get_type_name() ) );
      
   endtask // peek_mac
