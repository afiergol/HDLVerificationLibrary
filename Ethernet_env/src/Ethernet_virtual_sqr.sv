//                              -*- Mode: Verilog -*-
// Filename        : Ethernet_virtual_sqr.sv
// Description     : The virtual sequencer for Ethernet.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:59:00 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: Ethernet_virtual_sqr
//The Virtual Sequencer for Ethernet
class Ethernet_virtual_sqr extends uvm_virtual_sequencer;

   //Variable: port
   //TLM port to peek packets from mac_agent's monitor
   uvm_blocking_peek_port #(uvm_sequence_item) port[string];

   //Variable: mac_sqr
   mvc_sequencer mac_sqr;

   //Variable: phy_sqr
   mvc_sequencer phy_sqr;

   //Variable: cfg
   //The Ethernet environment configuration.
   Ethernet_config cfg;
     
   `uvm_component_utils_begin(Ethernet_virtual_sqr)
   `uvm_component_utils_end

   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction

   //Function: build_phase
   //It fetches the configuration.
   //The function also set the sequencer in SEQ_ARB_STRICT_FIFO mode.
   function void build_phase(uvm_phase phase);
      uvm_object tmp;
      
      super.build_phase(phase);
      
      //Set configuration
      if( get_config_object("cfg", tmp, 0) )
	assert( $cast(cfg, tmp) );
      
      if(cfg == null) begin
	 `uvm_fatal("NOCONFIG", "Ethernet_config not set for this component.")
      end
      set_arbitration(SEQ_ARB_STRICT_FIFO);

   endfunction // build_phase
   
endclass // Ethernet_virtual_sqr
