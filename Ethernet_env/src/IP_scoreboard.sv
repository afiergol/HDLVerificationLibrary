//                              -*- Mode: Verilog -*-
// Filename        : IP_scoreboard.sv
// Description     : The scoreboard checking IP packets.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:00:02 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: IP_scoreboard
//The scoreboard checking IP packets.
class IP_scoreboard extends scoreboard_fifos;

   //Group: Statistics

   //Variable: ip_checked
   //Number of checked packets.
   int ip_checked;

   //Variables: ip_errors
   //Number of found broken packets
   int ip_errors;
      
   `uvm_component_utils( IP_scoreboard )

   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction

   //Task: receive
   //The task checks IP checksum.
   virtual task receive(string port, uvm_sequence_item transaction_);
      eth_1g_device_ipv4_frame frame;
      bit[15:0] check_sum;
      byte unsigned mac_data[];
      
      if( ! $cast( frame, transaction_) ) `uvm_error("IP_SCOREBORD", "Wrong IP packet received" );
      ip_checked++;

      if(frame.hlen - 5 == frame.option.size() ) begin
	 mac_data = new [frame.hlen * 4];
	 {mac_data[0], mac_data[1], mac_data[2], mac_data[3]} = { frame.ver, frame.hlen, frame.service, frame.total_length};
	 {mac_data[4], mac_data[5], mac_data[6], mac_data[7]} = { frame.identification, frame.flag, frame.fragmentation_offset};
	 {mac_data[8], mac_data[9], mac_data[10], mac_data[11]} = { frame.time_to_live, frame.protocol, frame.header_checksum};
	 {mac_data[12], mac_data[13], mac_data[14], mac_data[15]} = { frame.source_ip_address };
	 {mac_data[16], mac_data[17], mac_data[18], mac_data[19]} = { frame.destination_ip_address};
	 foreach(frame.option[i])
	   {mac_data[20+i*4], mac_data[21+i*4], mac_data[22+i*4], mac_data[23+i*4]} = frame.option[i];
	 
	 check_sum = eth_ip_checksum::calc_ip_checksum( mac_data, mac_data.size() );
	 
	 if(check_sum != 0)
	   begin
	      `uvm_error("IP_SCOREBOARD", "Checksum Mismatch");
	      ip_errors++;
	   end
	 else
	   begin
	      `uvm_info("IP_SCOREBOARD", "Checksum Machtched", UVM_HIGH);
	   end
      end
      else begin
	 `uvm_error("IP_SCOREBOARD", "Wrong header length");
	 ip_errors++;
      end // else: !if(frame.hlen - 5 == frame.option.size() )
      
   endtask // receive

   //Function: convert2string
   //The function prints statistics of the scoreboard.
   virtual function string convert2string();
      convert2string = $sformatf( {"\nIP checked: %0d\n",
				   "IP checksum errors: %0d"},
				  ip_checked, ip_errors);
   endfunction // convert2string
   
   //Function: report_phase
   //It reports statistics.
   function void report_phase(uvm_phase phase);
      super.report_phase(phase);

      //Print statistics
      `uvm_info( {get_type_name(), " statistics"}, convert2string(), UVM_LOW);
      
   endfunction // report_phase
   
endclass // IP_scoreboard
