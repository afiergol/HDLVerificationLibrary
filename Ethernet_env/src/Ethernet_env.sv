//                              -*- Mode: Verilog -*-
// Filename        : Ethernet_env.sv
// Description     : Ethernet environment.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:55:15 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: Ethernet_env
//
//The environment of the Ethernet.

class Ethernet_env extends uvm_env;

   //Variable: mac_agent
   mvc_agent mac_agent;

   //Variable: phy_agent
   mvc_agent phy_agent;

   //Variable: sqr
   //Virtual sequencer
   Ethernet_virtual_sqr sqr;
   
   //Variable: ip_scoreboard
   Ethernet_scoreboard scoreboard;

   //Variable: mac_monitor
   VIP_monitor mac_monitor;
   
   //Variable: phy_monitor
   VIP_monitor phy_monitor;
   
   //Variable: cfg
   Ethernet_config cfg;

   `uvm_component_utils( Ethernet_env )

   // Function: new
   //
   // Creates a new <Ethernet_env> with the given instance ~name~ and parent. 
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //
   //It creates sub-components and configures them at the end.
   virtual function void build_phase(uvm_phase phase);
      uvm_object tmp;
      
      super.build_phase(phase);
      
      //Set configuration
      if( get_config_object("cfg", tmp, 0) )
	assert( $cast(cfg, tmp) );

      if(cfg == null) begin
	 `uvm_fatal("NOCONFIG", "Ethernet_config not set for this component")
      end

      //creation
      mac_agent = mvc_agent::type_id::create("mac_agent", this);
      phy_agent = mvc_agent::type_id::create("phy_agent", this);
      sqr = Ethernet_virtual_sqr::type_id::create( "sqr", this);

      mac_monitor = VIP_monitor::type_id::create("mac_monitor", this);
      phy_monitor = VIP_monitor::type_id::create("phy_monitor", this);
      scoreboard = Ethernet_scoreboard::type_id::create("scoreboard", this);
      
      
      //configuration
      set_config_object( "mac_agent*", mvc_config_base_id, cfg.macCfg, .clone(0) );
      set_config_object( "phy_agent*", mvc_config_base_id, cfg.phyCfg, .clone(0) );

      //MVC configuration for Ethernet_monitors
      set_config_object("mac_monitor*", mvc_config_base_id, cfg.macCfg, .clone(0) );
      set_config_object("phy_monitor*", mvc_config_base_id, cfg.phyCfg, .clone(0) );
      //normal configuration for Ethernet_monitors
      set_config_object("mac_monitor", "cfg", cfg.macMonCfg, .clone(0) );
      set_config_object("phy_monitor", "cfg", cfg.phyMonCfg, .clone(0) );
      
      set_config_object( "scoreboard", "cfg", cfg.scoreCfg, .clone(0) );

      set_config_object( "sqr", "cfg", cfg, .clone(0) );

      //Create ports in ports array and virtual sequencer
      foreach( cfg.macMonCfg.submonitors[ name ] ) begin
	 sqr.port[ name ] = new( name, sqr);
      end
      foreach( cfg.phyMonCfg.submonitors[ name ] ) begin
	 sqr.port[ name ] = new( name, sqr);
      end

   endfunction // build_phase
   
   //Function: connect_phase
   //
   //It connects analysis ports from the agents with the scoreboard.
   //It assigns sequencers in virtual sequencer <sqr>
   function void connect_phase(uvm_phase phase);

      ////////////////////////////////
      //port connection to scoreboard
      ////////////////////////////////
      foreach( scoreboard.ports[name] ) begin
	 string kind = name.substr(0, 3);

	 case(kind)
	   "mac_": begin
	      `uvm_info("connecting MAC analysis port", 
			$psprintf("%s -> %s",
				  mac_monitor.ap[name].get_full_name(),
				  scoreboard.ports[name].get_full_name() ),
			UVM_MEDIUM);
	      mac_monitor.ap[name].connect( scoreboard.ports[name] );
	   end

	   "phy_": begin
	      `uvm_info("connecting PHY analysis port", 
			$psprintf("%s -> %s",
				  phy_monitor.ap[name].get_full_name(),
				  scoreboard.ports[name].get_full_name() ),
			UVM_MEDIUM);
	      phy_monitor.ap[name].connect( scoreboard.ports[name] );
	   end
	   default: begin
	      `uvm_fatal("Connection error", {"Port ", name, " is neither 'mac_' nor 'phy_port'"} );
	   end
	 endcase // case (kind)

      end // foreach ( scoreboard.ports[name] )

      //////////////////////
      //Virtual sequencer
      //////////////////////
      sqr.mac_sqr = mac_agent.m_sequencer;
      sqr.phy_sqr = phy_agent.m_sequencer;

      ///////////////////////////////////////
      //port connection to virtual sequencer
      ///////////////////////////////////////
      
      //MAC
      foreach( cfg.macMonCfg.submonitors[ name ] ) begin
	 `uvm_info("connecting MAC peek port", 
		   $psprintf("%s -> %s",
			     mac_monitor.peekPorts[ name ].get_full_name(),
			     sqr.port[name].get_full_name() ),
		   UVM_MEDIUM);

	 sqr.port[ name ].connect( mac_monitor.peekPorts[ name ] );
      end

      //PHY
      foreach( cfg.phyMonCfg.submonitors[ name ] ) begin
	 `uvm_info("connecting PHY peek port", 
		   $psprintf("%s -> %s",
			     phy_monitor.peekPorts[ name ].get_full_name(),
			     sqr.port[name].get_full_name() ),
		   UVM_MEDIUM);
	 
	 sqr.port[name].connect( phy_monitor.peekPorts[ name ] );
      end

   endfunction // connect_phase
   
endclass // Ethernet_env
