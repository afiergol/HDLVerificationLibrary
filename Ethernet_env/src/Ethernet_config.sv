//                              -*- Mode: Verilog -*-
// Filename        : Ethernet_config.sv
// Description     : The configuration class of the Ethernet environment.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:49:53 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

// Class: Ethernet_config
//
// The configuration class of the <Ethernet_env>.
class Ethernet_config extends uvm_object;

   //Typedef: MAC_CONFIG
   typedef eth_1g_vip_config MAC_CONFIG;

   //Typedef: PHY_CONFIG
   typedef eth_1g_vip_config PHY_CONFIG;
   
   //Variable: macCfg
   //MAC configuration.
   MAC_CONFIG macCfg;
   
   //Variable: phyCfg
   //PHY configuration.
   PHY_CONFIG phyCfg;

   //Variable: macMonCfg
   //MAC monitor configuration
   VIP_monitor_config macMonCfg;

   //Variable: phyMonCfg
   //PHY monitor configuration
   VIP_monitor_config phyMonCfg;
   
   //Variable: scoreCfg
   Ethernet_scoreboard_config scoreCfg;

   //Variable: dut_mac
   //MAC address of the DUT
   byte unsigned dut_mac[5:0];

   //Variable: dut_ip
   //IP address of the DUT
   int dut_ip;

  `uvm_object_utils( Ethernet_config );

   //Function: new
   //
   //Creates a new <Ethernet_Config> with the given instance ~name~.
   //It creates a new sub-configurations.
   function new( string name = "Ethernet_config" );
      super.new( name );
      phyCfg = new();
      macCfg = new();
      macMonCfg = VIP_monitor_config::type_id::create("macMonCfg");
      phyMonCfg = VIP_monitor_config::type_id::create("phyMonCfg");
      scoreCfg = Ethernet_scoreboard_config::type_id::create("scorecfg");
   endfunction // new

endclass
