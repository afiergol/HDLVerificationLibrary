//                              -*- Mode: Verilog -*-
// Filename        : Ethernet_scoreboard.sv
// Description     : The generic scoreboard containg scoreboards performing checks.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:57:40 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: Ethernet_scoreboard

//Class: Ethernet_scoreboard_config
//The class add no functionality with respect to scoreboard_main_base_config.
class Ethernet_scoreboard_config extends scoreboard_main_base_config;
   
   `uvm_object_utils_begin(Ethernet_scoreboard_config)
   `uvm_object_utils_end

   //Function: new
   function new(string name="Ethernet_scoreboard_config");
      super.new(name);
   endfunction
   
endclass // Ethernet_scoreboard_config


//Class: Ethernet_scoreboard
//The class performing IP analysis. It contains dedicated sub-scoreboards.
class Ethernet_scoreboard extends scoreboard_main_base;

   `uvm_component_utils( Ethernet_scoreboard )

   // Function: new
   // Creates a new <Ethernet_scoreboard> with the given instance ~name~ and parent. 
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

endclass // Ethernet_scoreboard
