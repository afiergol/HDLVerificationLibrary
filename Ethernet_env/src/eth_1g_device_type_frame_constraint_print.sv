//                              -*- Mode: Verilog -*-
// Filename        : eth_1g_device_type_frame_constraint_print.sv
// Description     : The class overrides do_print method.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 10:49:05 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: eth_1g_device_type_frame_constraint_print
//The class overrides the do_print() method. It skips the printing of payload information 
// leading to faster simulation time.
class eth_1g_device_type_frame_constraint_print extends eth_1g_device_type_frame;
   `uvm_object_utils(eth_1g_device_type_frame_constraint_print)

   //Function: new
   function new(string name="");
      super.new(name);
   endfunction // new

   //Function: do_print
   //The function doesn't print payload.
   function void do_print(uvm_printer printer);
      printer.print_string("Transaction Type", "type_frame" );
      printer.print_field("active_index", active_index, $bits(active_index));
      printer.print_array_header("dest_address",6);
      foreach(dest_address[i0_1])
        printer.print_field($psprintf("dest_address[%0d]",i0_1), dest_address[i0_1], $bits(dest_address[i0_1]));
      printer.print_array_footer();
      printer.print_array_header("src_address",6);
      foreach(src_address[i0_1])
        printer.print_field($psprintf("src_address[%0d]",i0_1), src_address[i0_1], $bits(src_address[i0_1]));
      printer.print_array_footer();
      printer.print_string("vlan_mode", vlan_mode.name());
      printer.print_field("vlan_tag", vlan_tag, $bits(vlan_tag));
      printer.print_array_header("mac_client_data",mac_client_data.size());
      printer.print_array_footer();
      printer.print_field("tx_error", tx_error, $bits(tx_error));
      printer.print_field("tx_error_on_cycle", tx_error_on_cycle, $bits(tx_error_on_cycle));
      printer.print_field("ext_error", ext_error, $bits(ext_error));
      printer.print_field("ext_error_on_cycle", ext_error_on_cycle, $bits(ext_error_on_cycle));
      printer.print_string("extension_mode", extension_mode.name());
      printer.print_string("type_mode", type_mode.name());
      printer.print_field("type_value", type_value, $bits(type_value));
   endfunction // do_print

endclass // eth_1g_device_type_frame_constraint_print

