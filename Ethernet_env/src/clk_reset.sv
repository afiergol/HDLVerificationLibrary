//                              -*- Mode: Verilog -*-
// Filename        : clk_reset.sv
// Description     : It generates clock and reset for GMII.
// Author          : Adrian Fiergolski
// Created On      : Fri May 3 12:49:53 2019
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

timeunit 1ns;
timeprecision 1ps;

//It generates clock and reset for GMII
//Inspired by Mentor's module.
module clk_reset (output bit clk0, clk1, reset);

   reg iclk, irst;
   
   assign clk0 = iclk;
   assign clk1 = iclk;
   assign reset = irst;

   initial
     begin
	irst = 1;
	repeat (20) @(posedge clk0);
	#10;
	irst = 0;
     end
   
   initial
     begin
	iclk = 0;
        forever #4000ps iclk = ~iclk;
     end
   
endmodule
