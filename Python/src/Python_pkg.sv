//                              -*- Mode: Verilog -*-
// Filename        : Python_pkg.sv
// Description     : Package enabling launch Python code from SV simulation.
// Author          : Adrian Fiergolski
// Created On      : Thu Apr 16 16:19:46 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifndef PYTHON_PKG_SV
 `define PYTHON_PKG_SV

package Python_pkg;
   import uvm_pkg::*;
 `include <uvm_macros.svh>
 
 `include "../../Utilities/src/DPIexports.sv"
 `include "runPythonScript_seq.sv"
   
endpackage // Python_pkg

`endif
