// Filename        : runPythonScript.cpp
// Description     : DPI functions to call Python scripts from simulation.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:00:02 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

#include <Python.h>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/atomic.hpp>
#include <svdpi.h>
#include <unistd.h>
#include <dlfcn.h>

extern "C"
{
#include "../../../Utilities/src/DPI/DPIUtilities.hpp"

  using namespace std;

  class PythonScript
  {
  public:
    PythonScript( const char* path) : path_(path)
    { 
      dynamicLibrary = dlopen("libpython2.7.so", RTLD_LAZY | RTLD_GLOBAL);
      Py_Initialize();
      
      workDone = 0;
      success = 0;
    }

    ~PythonScript()
    {
      Py_Finalize();
      dlclose(dynamicLibrary);
    }
    
    mutable boost::atomic<bool> workDone;
    bool success;

    int run()
    {
      try{
      if (PyRun_SimpleString("import sys") )
      	{
      	  cout << "Python error: import sys" << endl;
	  throw std::exception();
      	}
      string cmd = "sys.path.append(\"" + path_.parent_path().string() + "\")";
      if( PyRun_SimpleString( cmd.c_str() ) )
      	{
      	  cout << "Python error: " << cmd << endl;
	  throw std::exception();
      	};
      cmd = "__file__ = \"" + path_.string() + "\"";
      if( PyRun_SimpleString( cmd.c_str() ))
      	{
      	  cout << "Python error: " << cmd << endl;
	  throw std::exception();
      	}
      cmd = "execfile('" + path_.string() + "')";
      if( PyRun_SimpleString( cmd.c_str() ))
      	{
      	  cout << "Python error: " << cmd << endl;
	  throw std::exception();
      	}
      }
      catch( std::exception &e){
	workDone = 1;
	return -1;
      }
      workDone = 1;
      success = 1;
      return 0;
    }
    
  private:
    boost::filesystem::path path_;
    void * dynamicLibrary;
  };

  int runPythonScript_DPI(const char* path, long long sleep_period, int* success)
  {
    PythonScript *script = new PythonScript(path);
    boost::thread run_thread( boost::bind( &PythonScript::run, boost::ref(*script) ) );
    try
      {
	while(! script->workDone )  //wait for the Python thread to finish
	  if( svSleep( sleep_period ) )
	    throw taskDisabled();
	*success = script->success;
	delete script;
	if(svIsDisabledState())
	  throw taskDisabled();
	else
	  return 0;
      }
    catch (taskDisabled & e)
      {
	run_thread.interrupt();
	run_thread.join();
	svAckDisabledState();
	return 1;    //due to disabe state (SV standard)          
      }
  }
}
