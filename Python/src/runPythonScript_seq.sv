//                              -*- Mode: Verilog -*-
// Filename        : runPythonScript_seq.sv
// Description     : The sequences enables to launch Python script.
// Author          : Adrian Fiergolski
// Created On      : Thu Apr 16 16:22:45 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

import "DPI-C" context task runPythonScript_DPI(string path, time sleep_period, output int sucess);

//Class: runPythonScript_seq
//The sequence can launch Python script on any sequencer.
class runPythonScript_seq extends uvm_sequence_base;

   //Variable: script
   //The relative path to the script to be called.
   string script;

   //Variable: sleep_period
   //Period with which Python script status is checked.
   time   sleep_period = 100ns;
       
   `uvm_declare_p_sequencer( uvm_sequencer_base )
   `uvm_object_utils_begin(runPythonScript_seq)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <runPythonScript_seq> with the given ~name~.
   function new(string name="runPythonScript_seq");
      super.new(name);
   endfunction // new

   //Task: pre_body
   //It raises objection.
   task pre_body();
      if(starting_phase != null )
	starting_phase.raise_objection(this, $sformatf("Starting %s", get_name() ) );
   endtask // pre_body
   
   //Task: post_body
   //It drops objection.
   task post_body();
      if(starting_phase != null )
	starting_phase.drop_objection(this, $sformatf("Ending %s", get_name() ) );
   endtask // pre_body

   //Task: body
   //The task spawns the Python script.
   virtual task body();
      int success = 0;
      runPythonScript_DPI(script, sleep_period, success);
      if(! success )
	`uvm_error( "runPythonScript_seq", "Script execution failed");
   endtask // body
   
endclass // runPythonScript_seq
