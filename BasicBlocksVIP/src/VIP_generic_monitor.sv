//                              -*- Mode: Verilog -*-
// Filename        : VIP_generic_monitor.sv
// Description     : The generic VIP monitor.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:34:01 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: Generic monitor
//This part describes monitor able to receive specified kind of frame.
//It a kind of wrapper of the VIP monitors giving to the user access to the generic ports using uvm_sequence_item.
//Moreover, <VIP_generic_monitor>s supports sequence peek, ect.

//Class: VIP_generic_monitor_config
//The configuration class of the <VIP_generic_monitor>.
class VIP_generic_monitor_config extends monitor_base_config;

   //Variable: tr_wrapper
   //Type of transaction to be received from the VIP
   uvm_object_wrapper tr_wrapper;

   //Variable: associated_agent
   string associated_agent;
   
   `uvm_object_utils_begin(VIP_generic_monitor_config)
   `uvm_object_utils_end

   //Function: new
   function new(string name="VIP_generic_monitor_config");
      super.new(name);
   endfunction

   //Function: set_wrapper
   //It sets wrapper.
   function void set_wrapper(uvm_object_wrapper tr_wrapper);
      this.tr_wrapper = tr_wrapper;
   endfunction // set_tr

   //Function: set_associated_agent
   //It set associated_agent
   function void set_associated_agent( string associated_agent);
      this.associated_agent = associated_agent;
   endfunction // set_associated_agent
   
endclass // VIP_generic_monitor_config


//Class: VIP_generic_monitor
//The generic monitor receiving transaction basing on the type provided in configuration.
class VIP_generic_monitor extends monitor_base;

   //Variable: mvc_tr
   //Transaction used to access the interface.
   mvc_pkg::mvc_sequence_item_base mvc_tr;
   
   `uvm_component_utils(VIP_generic_monitor)

   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction

   //Function: build_phase
   //It build a <tr> basing on the given <cfg>.
   function void build_phase( uvm_phase phase );
      
      VIP_generic_monitor_config cfg_;
      uvm_factory f = uvm_factory::get();
      uvm_object temp;
      
      super.build_phase(phase);

      //fetch configuration
      assert( $cast(cfg_, monCfg) );
      if( cfg_ == null )
	`uvm_error("NOCONFIG", $sformatf("No configuration set for the %s", get_name() ) );

      temp = f.create_object_by_type( cfg_.tr_wrapper, get_full_name(), "tr");

      if( ! $cast( mvc_tr, temp ) )
	`uvm_error(" CAST_ERROR", "Wrong transaction handle");
      
      mvc_tr.m_receive_id = mvc_pkg::get_stream_id( this );

   endfunction // build_phase

   //Task: receive
   //It receives the given transaction from VIP and be default assigns the it to <tr> which is later announced.
   virtual task receive();
      mvc_tr.receive( mvc_pkg::mvc_config_base::get_config(this) );
      tr = mvc_tr;
   endtask // receive


endclass // VIP_generic_monitor
