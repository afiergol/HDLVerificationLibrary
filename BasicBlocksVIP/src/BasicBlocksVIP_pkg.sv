//                              -*- Mode: Verilog -*-
// Filename        : BasicBlocksVIP_pkg.sv
// Description     : The package of basic blocks useful for building a testbench based on Questa VIP.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 23 11:37:56 2018
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2018
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifdef BASICBLOCKSVIP_PKG_SV
 `define BASICBLOCKSVIP_PKG_SV

 `include <mvc_macros.svh>
 `include <questa_mvc_svapi.svh>
 `include <mvc_pkg.sv>

//Package: BasicBlocksVIP_pkg
//Package contains basic blocks useful to build quickly a testbench based on Questa VIP.

package BasicBlocksVIP_pkg;

   import uvm_pkg::*;
 `include <uvm_macros.svh>

   ///////////////////////////////////////////////////
   //The classes from the following files use mvc
   ///////////////////////////////////////////////////

   import mvc_pkg::*;
   
   //Typedef: VIP_scoreboard_config
   typedef scoreboard_main_base_config VIP_scoreboard_config;
   //Typedef: VIP_scoreboard
   typedef scoreboard_main_base VIP_scoreboard;

 `include "VIP_generic_monitor.sv"
 `include "VIP_monitor.sv"
 `include "VIP_config.sv"
 `include "VIP_virtual_sqr.sv"   
 `include "VIP_env.sv"
   
endpackage // BasicBlocksVIP_pkg

`endif
