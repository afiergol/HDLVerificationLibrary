//                              -*- Mode: Verilog -*-
// Filename        : VIP_monitor.sv
// Description     : The monitor containing several generic_monitors.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:34:32 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: VIP_monitor

//Class: VIP_monitor_config
//Configuration class of the <VIP_monitor>
class VIP_monitor_config extends uvm_object;

   //Variable: config_t
   //The configuration of the BFM
   mvc_pkg::mvc_config_base config_t;
   
   //Variable: submonitors
   //Associative array of sub-monitors to be build by the <VIP_monitor>.
   VIP_generic_monitor_config submonitors[string];
   
   `uvm_object_utils( VIP_monitor_config )

   //Function: new
   function new(string name="VIP_monitor_config");
      super.new(name);
   endfunction // new

   //Function: add_submonitor
   //It adds a new sub-monitor to the <VIP_monitor>.
   //Parameters:
   //- ~name~ - name of the created dedicated sub-monitor
   //- ~wrapper~ - wrapper of the transaction type, which should be returned by the given sub_monitor
   //- ~cfg~ - configuration for the sub-monitor
   function void add_submonitor( string name, uvm_object_wrapper wrapper, VIP_generic_monitor_config cfg = null, string associated_agent = name );
      if(cfg == null)
	cfg = VIP_generic_monitor_config::type_id::create( {name, "_cfg"} );
      cfg.set_wrapper( wrapper );
      cfg.set_associated_agent(associated_agent);
      submonitors[name] = cfg;
   endfunction // add_submonitor
   
endclass // VIP_monitor_config

//Class: VIP_monitor
//Class containing several <VIP_generic_monitor>s
class VIP_monitor extends uvm_monitor;

   //Variable: cfg
   //Configuration
   VIP_monitor_config monCfg;

   //Variable: submonitors
   VIP_generic_monitor submonitors[string];

   //Variable: ap
   //Associative array of analysis port.
   //The name is build of ~submonitorName~_ap.
   uvm_analysis_export #(uvm_sequence_item) ap[string];

   //Variable: peekPorts
   //Associative array of peek ports, which may by used by the virtual sequencer
   uvm_blocking_peek_export #(uvm_sequence_item) peekPorts[string];
   
   
   `uvm_component_utils_begin( VIP_monitor )
      `uvm_field_object(monCfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end
   

   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction

   //Function: build_phase
   //It creates the requested <VIP_generic_monitor>s and dedicated <ap> and <peekPorts> ports.
   function void build_phase( uvm_phase phase);

      super.build_phase(phase);

      if( monCfg == null ) begin
	 `uvm_warning( "NOCONFIG", "There is no configuration, creating the default one");
	 monCfg = VIP_monitor_config::type_id::create("monCfg");
      end

      foreach( monCfg.submonitors[name] ) begin
	 submonitors[name] = VIP_generic_monitor::type_id::create( name, this);
	 set_config_object( name, "monCfg", monCfg.submonitors[name], .clone(0) );
	 if( monCfg.submonitors[name].ap_enabled )
	   ap[name] = new( {name, "_ap"}, this );
	 peekPorts[name] = new( {name, "_peek"}, this);
      end
      
   endfunction // build_phase

   //Function: connect_phase
   //It connects the sub-monitors to the <VIP_monitor>.
   function void connect_phase( uvm_phase phase);
      super.connect_phase(phase);

      foreach( submonitors[name] ) begin
	 if( submonitors[name].ap != null ) begin
	    `uvm_info("connecting analysis port", 
		      $sformatf("%s -> %s", name,
				ap[name].get_full_name() ),
		      UVM_MEDIUM);
	    submonitors[name].ap.connect( ap[name] );
	 end

	 `uvm_info("connecting peek port", 
		   $sformatf("%s -> %s", name,
			     peekPorts[name].get_full_name() ),
		   UVM_MEDIUM);
	 peekPorts[name].connect( submonitors[name].peekPort );
      end

   endfunction // connect_phase

endclass // VIP_monitor
