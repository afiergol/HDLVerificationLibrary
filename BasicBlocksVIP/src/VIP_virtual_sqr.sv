//                              -*- Mode: Verilog -*-
// Filename        : VIP_virtual_sqr.sv
// Description     : VIP generic virtual sequenver.
// Author          : Adrian Fiergolski
// Created On      : Tue Nov  3 12:53:56 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: VIP_virtual_sqr
//The virtual sequncer for VIP.
class VIP_virtual_sqr extends uvm_virtual_sequencer;

   //Variable: port
   //TLM port to peek packets from mac_agent's monitor
   uvm_blocking_peek_port #(uvm_sequence_item) port[string];
   
   //Variable:sqr
   mvc_sequencer sqr[string];

   //Variable: cfg
   VIP_config cfg;
   
   `uvm_component_utils_begin(VIP_virtual_sqr)
      `uvm_field_aa_object_string(sqr, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <VIP_virtual_sqr> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //It creates <ports>
   virtual function void build_phase(uvm_phase phase);
      
      super.build_phase(phase);
      
      if(cfg == null) begin
	 `uvm_fatal("NOCONFIG", "VIP_config not set for this component")
      end

      foreach( cfg.monCfg.submonitors[name] )
	port[ name ] = new( name, this);

   endfunction // build_phase

endclass // VIP_virtual_sqr
