//                              -*- Mode: Verilog -*-
// Filename        : VIP_env.sv
// Description     : Generic VIP environment.
// Author          : Adrian Fiergolski
// Created On      : Tue Nov  3 12:48:55 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: VIP_env
//The generic VIP environment.
class VIP_env extends uvm_env;

   //Variable: agent
   mvc_agent agent[string];

   //Variable: sqr
   VIP_virtual_sqr sqr;

   //Variable: monitor
   VIP_monitor monitor;

   //Variable: scoreboard
   VIP_scoreboard scoreboard;

   //Variable: cfg
   VIP_config cfg;
   
   `uvm_component_utils_begin(VIP_env)
      `uvm_field_aa_object_string(agent, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(sqr, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(monitor, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(scoreboard, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <VIP_env> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //
   //It creates sub-components and configures them at the end.
   virtual function void build_phase(uvm_phase phase);

      super.build_phase(phase);
      
      //check configuration
      if(cfg == null) begin
	 `uvm_fatal("NOCONFIG", "VIP_config not set for this component")
      end

      //creation
      foreach( cfg.agentCfg[name] )
	agent[name] =  mvc_agent::type_id::create( {name, "_agent"} , this);
      sqr = VIP_virtual_sqr::type_id::create( "sqr", this);

      monitor = VIP_monitor::type_id::create("monitor", this);
      scoreboard = VIP_scoreboard::type_id::create("scoreboard", this);

      //configuration
      foreach( cfg.agentCfg[name] )
	set_config_object( {name, "_agent*"}, mvc_config_base_id, cfg.agentCfg[name], .clone(0) );

      set_config_object("monitor", "monCfg", cfg.monCfg, .clone(0) );

      foreach( cfg.monCfg.submonitors[name] )
	if( cfg.agentCfg.exists( cfg.monCfg.submonitors[name].associated_agent ) )
	  set_config_object( {"monitor.", name , "*"} , mvc_config_base_id, cfg.agentCfg[ cfg.monCfg.submonitors[name].associated_agent ], .clone(0) );
	else
	  `uvm_fatal( "NOCONFIG", $psprintf( "Submonitor %s is missing an agent configuration.", name ) );

      set_config_object( "scoreboard", "cfg", cfg.scoreCfg, .clone(0) );

      set_config_object( "sqr", "cfg", cfg, .clone(0) );

   endfunction // build_phase

   //Function: connect_phase
   //
   //It connects analysis ports from the agents with the scoreboard.
   //It assigns sequencers in virtual sequencer <sqr>
   function void connect_phase(uvm_phase phase);

      ////////////////////////////////
      //port connection to scoreboard
      ////////////////////////////////

      foreach( scoreboard.ports[name] ) begin
	 `uvm_info("connecting analysis port", 
			$psprintf("%s -> %s",
				  monitor.ap[name].get_full_name(),
				  scoreboard.ports[name].get_full_name() ),
		   UVM_MEDIUM);
	 if( monitor.ap.exists(name) )
	   monitor.ap[name].connect( scoreboard.ports[name] );
	 else
	   `uvm_fatal( "NOPORT", $psprintf( "Monitor %s is missing port %s", monitor.get_full_name(), name ) );
      end // foreach ( scoreboard.ports[name] )

      //////////////////////////////
      //Virtual sequencers
      //////////////////////////////
      foreach( agent[name] )
	sqr.sqr[name] = agent[name].m_sequencer;

      //port connection to virtual sequencer
      foreach( monitor.peekPorts[name] ) begin
	`uvm_info("connecting peek port", 
		  $psprintf("%s -> %s",
			    monitor.peekPorts[ name ].get_full_name(),
			    sqr.port[name].get_full_name() ),
		  UVM_MEDIUM);
	 if( sqr.port.exists(name) )
	   sqr.port[ name ].connect( monitor.peekPorts[ name ] );
	 else
	   `uvm_fatal( "NOPORT", $psprintf( "Sequencer %s is missing port %s", sqr.get_full_name(), name ) );
      end // foreach ( monitor.peekPorts[name] )
      
   endfunction // connect_phase

endclass // VIP_env
