//                              -*- Mode: Verilog -*-
// Filename        : VIP_config.sv
// Description     : The configuration class of the VIP environment.
// Author          : Adrian Fiergolski
// Created On      : Tue Nov  3 13:11:11 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: VIP_config
//The configuration class of the <VIP_env>.
class VIP_config extends uvm_object;

   //Variable: agentCfg
   //Configuration of the agents
   uvm_object agentCfg[string];

   //Variable: monCfg
   //Monitor configuration
   VIP_monitor_config monCfg;
   
   //Variable: scoreCfg
   VIP_scoreboard_config scoreCfg;
   
   `uvm_object_utils_begin(VIP_config)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <VIP_config> with the given ~name~.
   function new(string name="VIP_config");
      super.new(name);

   endfunction // new

   //Function: createDefault
   //It creates default subconfiguration elements.
   virtual function void createDefault(uvm_component parent = null);
      monCfg = VIP_monitor_config::type_id::create("monCfg" , parent);
      scoreCfg = VIP_scoreboard_config::type_id::create("scorecfg", parent);
   endfunction // createDefault
   
endclass // VIP_config
