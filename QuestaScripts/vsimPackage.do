 # Filename        : vsimPacakge.do
 # Description     : Set of procuders useful run Questa simulation.
 # Author          : Adrian Fiergolski
 # Created On      : Wed Feb  24 10:58:00 2016

 # Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016

 # This source file is licensed under the CERN OHL v. 1.2.

 # You may redistribute and modify this souce file under the terms of the
 # CERN OHL v.1.2. (http:ohwr.org/cernohl). This project is distributed
 # WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
 # SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
 # the CERN OHL v.1.2 for applicable conditions.

package provide vsimPackage 1.0

namespace eval ::vsimPackage {
    #Export Commands
    namespace export start_vsim compile run_test generate_arguments_from_file
}

proc ::vsimPackage::start_vsim {top_level test_case {vopt "novopt"} {sdftyp ""} {voptargs ""} {vsim_param "-classdebug -uvmcontrol=all -msgmode both -t ps -quiet"} } {
    set parameters {}
    set parameters [split $vsim_param " "]
    lappend parameters "-$vopt"

    if { $sdftyp != "" } {
	lappend parameters {*}$sdftyp
    }
    
    if { $voptargs != "" } {
	lappend parameters [concat "-voptargs=" $voptargs ""]
    }
    
    lappend parameters  "+UVM_TESTNAME=${test_case}" ${top_level}
    
    vsim {*}$parameters
}

proc ::vsimPackage::compile_design { {design "rtl"} {clean_before ""} } {
    if { $clean_before == "1" } {
	echo [exec make clean]
    }

    set ::env(PYTHONPATH) $::env(hdlmake_path)
    if { [ catch {exec python -m hdlmake -p "${design}=None" makefile } hdlMake_msg ] }    {
	echo $hdlMake_msg
    }
    after 1000
    echo [exec make]
}

proc ::vsimPackage::generate_arguments_from_file { {file_name ""} {prefix_command ""} {replaced_string ""} {replacing_string ""} } {

    set fp [open $file_name r]
    set file_lines [split [read $fp] "\n"]
    set args {}
    foreach line $file_lines {
	lappend args $prefix_command [regsub -- [join [list "(\[^\.]*)" RTL_PATH "(\[^\.]*)" ] "" ] $line [join [list \\1 $replacing_string \\2 ] "" ] ]
    }
    return $args
}


proc ::vsimPackage::run_test { top_level test_case {wave ""} {vopt "novopt"} {design "rtl"} {clean_before ""}  {sdftyp ""} {voptargs ""} {vsim_param "-classdebug -uvmcontrol=all -msgmode both -t ps -quiet"} {script ""}  } {
    
    ### Preparation 
    quit -sim

    .main clear
    transcript file ""
    transcript file transcript
    
    compile_design $design $clean_before

    ### Ignore any sdf assignments if RTL
    if { $design == "rtl"} {
	set sdftyp ""
    }
    
    start_vsim $top_level $test_case $vopt $sdftyp $voptargs $vsim_param

    quietly set NumericStdNoWarnings 1;
    quietly set StdArithNoWarnings 1;
    quietly set IgnoreNote 1;
    #1 step
    run 

    ### Enable wave
    if {$wave != ""} {
	do $wave
    }
    
    quietly set NumericStdNoWarnings 0;
    quietly set StdArithNoWarnings 0;
    foreach cmd $script { {*}$cmd }
    run -all
}

