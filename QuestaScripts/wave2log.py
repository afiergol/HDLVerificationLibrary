#!/usr/bin/python
import re, sys

with open(sys.argv[1], 'r') as wave:
    with open(sys.argv[2], 'w') as log:
        regex = re.compile('^add wave -noupdate ([\w /]+)')
        for line in wave:
            signal = regex.search(line)
            if signal != None:
                log.write('catch { log ' + signal.group(1) + ' }\n')
