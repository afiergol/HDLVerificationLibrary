//                              -*- Mode: Verilog -*-
// Filename        : SerDes_inter.sv
// Description     : SerDes interface.
// Author          : Adrian Fiergolski
// Created On      : Tue Mar  8 18:05:08 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Interface: SerDes_inter
//Interface for SerDes link.

interface SerDes_inter;

   timeunit 1ps;
   timeprecision 1ps;

   //Variable: link
   //The data line
   logic link;

   //Variable: sample_clk
   wire  sample_clk;

   //Clocking: cbs
   //Slave clocking block (used also by the monitor).
   clocking cbs @(posedge sample_clk);
      input link;
   endclocking // cbs

   //Variable: config_GenerateSampleClock
   bit 	 config_GenerateSampleClock = 0;

   //Variable: config_SampleClockPeriod
   time  config_SampleClockPeriod = 1ns;

   logic sample_clk_i = 'bz;
   logic sample_clk_ext = 'bz;
   initial begin : InternalClockGeneration
     forever
       if( config_GenerateSampleClock ) begin
	  sample_clk_i <= 0;
	  #(config_SampleClockPeriod) sample_clk_i <= ~ sample_clk_i;
	end
       else begin
	  sample_clk_i <= 'bz;
	  @(config_GenerateSampleClock);
       end
   end
   assign sample_clk = sample_clk_i;
   assign sample_clk = sample_clk_ext;

   //Some basic assertions.
   UNDEFINED_X_STATE_ON_THE_LINK: assert property ( @(link) (link !== 1'bx) && (link !== 1'bz) );
   
endinterface // SerDes_inter

