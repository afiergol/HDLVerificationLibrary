//                              -*- Mode: Verilog -*-
// Filename        : SerDes_monitor.sv
// Description     : The root class for SerDes monitor.
// Author          : Adrian Fiergolski
// Created On      : Mon Mar 14 17:00:40 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: SerDes monitor

// Class: SerDes_monitor
// It is the base class for all SerDes monitors.
virtual class SerDes_monitor extends monitor_base;

   // Variable: inter
   // A virtual interface of the <SerDes_inter>.
   vSerDes_inter inter;

   `uvm_component_utils( SerDes_monitor )

   // Function: new
   // Create a new <SerDes_monitor> with the given instance ~name~ and parent. 
   // If ~name~ is not supplied, the driver is unnamed.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   // Function: build_phase
   // Implements UVM build_phase.
   // It assigns interface.
   virtual function void  build_phase(uvm_phase phase);
      SerDes_monitor_config cfg_;
      super.build_phase(phase);
      assert( $cast( cfg_, monCfg) );
      inter = cfg_.inter;
   endfunction // build_phase

endclass // SerDes_monitor
