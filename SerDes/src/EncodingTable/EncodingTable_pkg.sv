//                              -*- Mode: Verilog -*-
// Filename        : EncodingTable_pkg.sv
// Description     : Pacakge containing encoding tables for SerDes.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 10 16:53:12 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifndef ENCODINGTABLE_PKG_SV
 `define ENCODINGTABLE_PKG_SV

package EncodingTable_pkg;

   import uvm_pkg::*;
 `include <uvm_macros.svh>

   // Typedef: WORD_TYPE
   //
   // A base data word which is processed by the SERDES_driver. 
   // The ~is_control~ indicates specific command ~word~.
   
   typedef struct packed{
      bit 	  is_control;
      byte 	  word;} WORD_TYPE;

   //Typedef: ENCODED_WORD_TYPE;
   //Type of the encoded word.
   typedef bit [0:9] ENCODED_WORD_TYPE;
   
 `include "EncodingTable_8b10b.sv"
 `include "EncodingTable_8b10b_802_3.sv"
   
endpackage // EncodingTable_pkg

`endif
