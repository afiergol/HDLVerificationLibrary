//                              -*- Mode: Verilog -*-
// Filename        : EncodingTable_8b10b_802_3.sv
// Description     : Encoding table following 802.3 standard
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 10 16:54:56 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.


// Class: EncodingTable_8b10b_802_3
// Implements <EncodingTable_8b10b> according to the 802.3 standard.
class  EncodingTable_8b10b_802_3 extends  EncodingTable_8b10b;

   //Variable: code_table
   //
   // Array of data codes.
   // 10b = code_table[8b][disparity]
   
   protected const static ENCODED_WORD_TYPE code_table[][2]='{
				   '{10'b1001110100, 10'b0110001011},
				   '{10'b0111010100, 10'b1000101011},
				   '{10'b1011010100, 10'b0100101011},
				   '{10'b1100011011, 10'b1100010100},
				   '{10'b1101010100, 10'b0010101011},
				   '{10'b1010011011, 10'b1010010100},
				   '{10'b0110011011, 10'b0110010100},
				   '{10'b1110001011, 10'b0001110100},
				   '{10'b1110010100, 10'b0001101011},
				   '{10'b1001011011, 10'b1001010100},
				   '{10'b0101011011, 10'b0101010100},
				   '{10'b1101001011, 10'b1101000100},
				   '{10'b0011011011, 10'b0011010100},
				   '{10'b1011001011, 10'b1011000100},
				   '{10'b0111001011, 10'b0111000100},
				   '{10'b0101110100, 10'b1010001011},
				   '{10'b0110110100, 10'b1001001011},
				   '{10'b1000111011, 10'b1000110100},
				   '{10'b0100111011, 10'b0100110100},
				   '{10'b1100101011, 10'b1100100100},
				   '{10'b0010111011, 10'b0010110100},
				   '{10'b1010101011, 10'b1010100100},
				   '{10'b0110101011, 10'b0110100100},
				   '{10'b1110100100, 10'b0001011011},
				   '{10'b1100110100, 10'b0011001011},
				   '{10'b1001101011, 10'b1001100100},
				   '{10'b0101101011, 10'b0101100100},
				   '{10'b1101100100, 10'b0010011011},
				   '{10'b0011101011, 10'b0011100100},
				   '{10'b1011100100, 10'b0100011011},
				   '{10'b0111100100, 10'b1000011011},
				   '{10'b1010110100, 10'b0101001011},
				   '{10'b1001111001, 10'b0110001001},
				   '{10'b0111011001, 10'b1000101001},
				   '{10'b1011011001, 10'b0100101001},
				   '{10'b1100011001, 10'b1100011001},
				   '{10'b1101011001, 10'b0010101001},
				   '{10'b1010011001, 10'b1010011001},
				   '{10'b0110011001, 10'b0110011001},
				   '{10'b1110001001, 10'b0001111001},
				   '{10'b1110011001, 10'b0001101001},
				   '{10'b1001011001, 10'b1001011001},
				   '{10'b0101011001, 10'b0101011001},
				   '{10'b1101001001, 10'b1101001001},
				   '{10'b0011011001, 10'b0011011001},
				   '{10'b1011001001, 10'b1011001001},
				   '{10'b0111001001, 10'b0111001001},
				   '{10'b0101111001, 10'b1010001001},
				   '{10'b0110111001, 10'b1001001001},
				   '{10'b1000111001, 10'b1000111001},
				   '{10'b0100111001, 10'b0100111001},
				   '{10'b1100101001, 10'b1100101001},
				   '{10'b0010111001, 10'b0010111001},
				   '{10'b1010101001, 10'b1010101001},
				   '{10'b0110101001, 10'b0110101001},
				   '{10'b1110101001, 10'b0001011001},
				   '{10'b1100111001, 10'b0011001001},
				   '{10'b1001101001, 10'b1001101001},
				   '{10'b0101101001, 10'b0101101001},
				   '{10'b1101101001, 10'b0010011001},
				   '{10'b0011101001, 10'b0011101001},
				   '{10'b1011101001, 10'b0100011001},
				   '{10'b0111101001, 10'b1000011001},
				   '{10'b1010111001, 10'b0101001001},
				   '{10'b1001110101, 10'b0110000101},
				   '{10'b0111010101, 10'b1000100101},
				   '{10'b1011010101, 10'b0100100101},
				   '{10'b1100010101, 10'b1100010101},
				   '{10'b1101010101, 10'b0010100101},
				   '{10'b1010010101, 10'b1010010101},
				   '{10'b0110010101, 10'b0110010101},
				   '{10'b1110000101, 10'b0001110101},
				   '{10'b1110010101, 10'b0001100101},
				   '{10'b1001010101, 10'b1001010101},
				   '{10'b0101010101, 10'b0101010101},
				   '{10'b1101000101, 10'b1101000101},
				   '{10'b0011010101, 10'b0011010101},
				   '{10'b1011000101, 10'b1011000101},
				   '{10'b0111000101, 10'b0111000101},
				   '{10'b0101110101, 10'b1010000101},
				   '{10'b0110110101, 10'b1001000101},
				   '{10'b1000110101, 10'b1000110101},
				   '{10'b0100110101, 10'b0100110101},
				   '{10'b1100100101, 10'b1100100101},
				   '{10'b0010110101, 10'b0010110101},
				   '{10'b1010100101, 10'b1010100101},
				   '{10'b0110100101, 10'b0110100101},
				   '{10'b1110100101, 10'b0001010101},
				   '{10'b1100110101, 10'b0011000101},
				   '{10'b1001100101, 10'b1001100101},
				   '{10'b0101100101, 10'b0101100101},
				   '{10'b1101100101, 10'b0010010101},
				   '{10'b0011100101, 10'b0011100101},
				   '{10'b1011100101, 10'b0100010101},
				   '{10'b0111100101, 10'b1000010101},
				   '{10'b1010110101, 10'b0101000101},
				   '{10'b1001110011, 10'b0110001100},
				   '{10'b0111010011, 10'b1000101100},
				   '{10'b1011010011, 10'b0100101100},
				   '{10'b1100011100, 10'b1100010011},
				   '{10'b1101010011, 10'b0010101100},
				   '{10'b1010011100, 10'b1010010011},
				   '{10'b0110011100, 10'b0110010011},
				   '{10'b1110001100, 10'b0001110011},
				   '{10'b1110010011, 10'b0001101100},
				   '{10'b1001011100, 10'b1001010011},
				   '{10'b0101011100, 10'b0101010011},
				   '{10'b1101001100, 10'b1101000011},
				   '{10'b0011011100, 10'b0011010011},
				   '{10'b1011001100, 10'b1011000011},
				   '{10'b0111001100, 10'b0111000011},
				   '{10'b0101110011, 10'b1010001100},
				   '{10'b0110110011, 10'b1001001100},
				   '{10'b1000111100, 10'b1000110011},
				   '{10'b0100111100, 10'b0100110011},
				   '{10'b1100101100, 10'b1100100011},
				   '{10'b0010111100, 10'b0010110011},
				   '{10'b1010101100, 10'b1010100011},
				   '{10'b0110101100, 10'b0110100011},
				   '{10'b1110100011, 10'b0001011100},
				   '{10'b1100110011, 10'b0011001100},
				   '{10'b1001101100, 10'b1001100011},
				   '{10'b0101101100, 10'b0101100011},
				   '{10'b1101100011, 10'b0010011100},
				   '{10'b0011101100, 10'b0011100011},
				   '{10'b1011100011, 10'b0100011100},
				   '{10'b0111100011, 10'b1000011100},
				   '{10'b1010110011, 10'b0101001100},
				   '{10'b1001110010, 10'b0110001101},
				   '{10'b0111010010, 10'b1000101101},
				   '{10'b1011010010, 10'b0100101101},
				   '{10'b1100011101, 10'b1100010010},
				   '{10'b1101010010, 10'b0010101101},
				   '{10'b1010011101, 10'b1010010010},
				   '{10'b0110011101, 10'b0110010010},
				   '{10'b1110001101, 10'b0001110010},
				   '{10'b1110010010, 10'b0001101101},
				   '{10'b1001011101, 10'b1001010010},
				   '{10'b0101011101, 10'b0101010010},
				   '{10'b1101001101, 10'b1101000010},
				   '{10'b0011011101, 10'b0011010010},
				   '{10'b1011001101, 10'b1011000010},
				   '{10'b0111001101, 10'b0111000010},
				   '{10'b0101110010, 10'b1010001101},
				   '{10'b0110110010, 10'b1001001101},
				   '{10'b1000111101, 10'b1000110010},
				   '{10'b0100111101, 10'b0100110010},
				   '{10'b1100101101, 10'b1100100010},
				   '{10'b0010111101, 10'b0010110010},
				   '{10'b1010101101, 10'b1010100010},
				   '{10'b0110101101, 10'b0110100010},
				   '{10'b1110100010, 10'b0001011101},
				   '{10'b1100110010, 10'b0011001101},
				   '{10'b1001101101, 10'b1001100010},
				   '{10'b0101101101, 10'b0101100010},
				   '{10'b1101100010, 10'b0010011101},
				   '{10'b0011101101, 10'b0011100010},
				   '{10'b1011100010, 10'b0100011101},
				   '{10'b0111100010, 10'b1000011101},
				   '{10'b1010110010, 10'b0101001101},
				   '{10'b1001111010, 10'b0110001010},
				   '{10'b0111011010, 10'b1000101010},
				   '{10'b1011011010, 10'b0100101010},
				   '{10'b1100011010, 10'b1100011010},
				   '{10'b1101011010, 10'b0010101010},
				   '{10'b1010011010, 10'b1010011010},
				   '{10'b0110011010, 10'b0110011010},
				   '{10'b1110001010, 10'b0001111010},
				   '{10'b1110011010, 10'b0001101010},
				   '{10'b1001011010, 10'b1001011010},
				   '{10'b0101011010, 10'b0101011010},
				   '{10'b1101001010, 10'b1101001010},
				   '{10'b0011011010, 10'b0011011010},
				   '{10'b1011001010, 10'b1011001010},
				   '{10'b0111001010, 10'b0111001010},
				   '{10'b0101111010, 10'b1010001010},
				   '{10'b0110111010, 10'b1001001010},
				   '{10'b1000111010, 10'b1000111010},
				   '{10'b0100111010, 10'b0100111010},
				   '{10'b1100101010, 10'b1100101010},
				   '{10'b0010111010, 10'b0010111010},
				   '{10'b1010101010, 10'b1010101010},
				   '{10'b0110101010, 10'b0110101010},
				   '{10'b1110101010, 10'b0001011010},
				   '{10'b1100111010, 10'b0011001010},
				   '{10'b1001101010, 10'b1001101010},
				   '{10'b0101101010, 10'b0101101010},
				   '{10'b1101101010, 10'b0010011010},
				   '{10'b0011101010, 10'b0011101010},
				   '{10'b1011101010, 10'b0100011010},
				   '{10'b0111101010, 10'b1000011010},
				   '{10'b1010111010, 10'b0101001010},
				   '{10'b1001110110, 10'b0110000110},
				   '{10'b0111010110, 10'b1000100110},
				   '{10'b1011010110, 10'b0100100110},
				   '{10'b1100010110, 10'b1100010110},
				   '{10'b1101010110, 10'b0010100110},
				   '{10'b1010010110, 10'b1010010110},
				   '{10'b0110010110, 10'b0110010110},
				   '{10'b1110000110, 10'b0001110110},
				   '{10'b1110010110, 10'b0001100110},
				   '{10'b1001010110, 10'b1001010110},
				   '{10'b0101010110, 10'b0101010110},
				   '{10'b1101000110, 10'b1101000110},
				   '{10'b0011010110, 10'b0011010110},
				   '{10'b1011000110, 10'b1011000110},
				   '{10'b0111000110, 10'b0111000110},
				   '{10'b0101110110, 10'b1010000110},
				   '{10'b0110110110, 10'b1001000110},
				   '{10'b1000110110, 10'b1000110110},
				   '{10'b0100110110, 10'b0100110110},
				   '{10'b1100100110, 10'b1100100110},
				   '{10'b0010110110, 10'b0010110110},
				   '{10'b1010100110, 10'b1010100110},
				   '{10'b0110100110, 10'b0110100110},
				   '{10'b1110100110, 10'b0001010110},
				   '{10'b1100110110, 10'b0011000110},
				   '{10'b1001100110, 10'b1001100110},
				   '{10'b0101100110, 10'b0101100110},
				   '{10'b1101100110, 10'b0010010110},
				   '{10'b0011100110, 10'b0011100110},
				   '{10'b1011100110, 10'b0100010110},
				   '{10'b0111100110, 10'b1000010110},
				   '{10'b1010110110, 10'b0101000110},
				   '{10'b1001110001, 10'b0110001110},
				   '{10'b0111010001, 10'b1000101110},
				   '{10'b1011010001, 10'b0100101110},
				   '{10'b1100011110, 10'b1100010001},
				   '{10'b1101010001, 10'b0010101110},
				   '{10'b1010011110, 10'b1010010001},
				   '{10'b0110011110, 10'b0110010001},
				   '{10'b1110001110, 10'b0001110001},
				   '{10'b1110010001, 10'b0001101110},
				   '{10'b1001011110, 10'b1001010001},
				   '{10'b0101011110, 10'b0101010001},
				   '{10'b1101001110, 10'b1101001000},
				   '{10'b0011011110, 10'b0011010001},
				   '{10'b1011001110, 10'b1011001000},
				   '{10'b0111001110, 10'b0111001000},
				   '{10'b0101110001, 10'b1010001110},
				   '{10'b0110110001, 10'b1001001110},
				   '{10'b1000110111, 10'b1000110001},
				   '{10'b0100110111, 10'b0100110001},
				   '{10'b1100101110, 10'b1100100001},
				   '{10'b0010110111, 10'b0010110001},
				   '{10'b1010101110, 10'b1010100001},
				   '{10'b0110101110, 10'b0110100001},
				   '{10'b1110100001, 10'b0001011110},
				   '{10'b1100110001, 10'b0011001110},
				   '{10'b1001101110, 10'b1001100001},
				   '{10'b0101101110, 10'b0101100001},
				   '{10'b1101100001, 10'b0010011110},
				   '{10'b0011101110, 10'b0011100001},
				   '{10'b1011100001, 10'b0100011110},
				   '{10'b0111100001, 10'b1000011110},
				   '{10'b1010110001, 10'b0101001110}};

   // Variable: control_code_table
   //
   // Array of special control words.
   // 10b = control_code_table[8b][disparity]
      
   protected const static ENCODED_WORD_TYPE control_code_table [ byte unsigned ][2] = '{
				   'h1C : '{10'b0011110100, 10'b1100001011},
				   'h3C : '{10'b0011111001, 10'b1100000110},
 				   'h5C : '{10'b0011110101, 10'b1100001010},
				   'h7C : '{10'b0011110011, 10'b1100001100},
				   'h9C : '{10'b0011110010, 10'b1100001101},
				   'hBC : '{10'b0011111010, 10'b1100000101},
				   'hDC : '{10'b0011110110, 10'b1100001001},
				   'hFC : '{10'b0011111000, 10'b1100000111},
				   'hF7 : '{10'b1110101000, 10'b0001010111},
				   'hFB : '{10'b1101101000, 10'b0010010111},
				   'hFD : '{10'b1011101000, 10'b0100010111},
				   'hFE : '{10'b0111101000, 10'b1000010111}};
   
			   
   // Variable: code_table_rev
   //
   // Reverse array of used encoding.
   // It contains data and specific control words. Is generated in the constructor, when first instance of the class is created. In case of wrong code {1, '1} is returned.
   // 8b = code_table_rev[disparity][10b]
   
   protected static WORD_TYPE code_table_rev[2][ ENCODED_WORD_TYPE ] = '{ 2{'{default:'1}} };

   //Variable: COMMA_PATTERN_P
   const ENCODED_WORD_TYPE COMMA_PATTERN_P;
   //Variable: COMMA_PATTERN_N
   const ENCODED_WORD_TYPE COMMA_PATTERN_N;
   //Variable: COMMA_MASK
   const ENCODED_WORD_TYPE COMMA_MASK;

   `uvm_object_utils(  EncodingTable_8b10b_802_3 )

   // Function: new
   //
   // Creates a new <EncodingTable_8b10b_802_3> with the given instance ~name~. 
   // If ~name~ is not supplied, the object is unnamed. For the first instance of the class, the <crate_code_table_rev> is executed.
   
   function new(string name = "");
      super.new(name);
      if(! code_table_rev[1].num() ) //code_table_rev has not been created
	create_code_table_rev();
      COMMA_PATTERN_N = control_table( {1'b1, 8'hBC}, 0);
      COMMA_PATTERN_P = control_table( {1'b1, 8'hBC}, 1);
      COMMA_MASK = 'b1111111000;    //only 7 bits are used to as comma
      IDLE_LENGTH = 2;
   endfunction // new

   // Function: create_code_table_rev
   // Create the <code_table_rev>
   
   protected static function void create_code_table_rev();
      //code tables
      foreach(code_table[i,j])
	code_table_rev[j][ code_table[i][j] ] = '{0, i};
     
      //special command words table
      foreach(control_code_table[i,j])
	code_table_rev[j][ control_code_table[i][j] ] = '{1, i};
      
   endfunction // create_code_table_rev
   
   // Function: encoding_table
   // Return the 8b10 code for a given ~word~ and ~disparity~.
   
   virtual function ENCODED_WORD_TYPE encoding_table(input byte word, input bit disparity);
      return code_table[unsigned'(word)][disparity];
   endfunction // encoding_table
    

   //Function: control_table
   // Return codes for a control words according to 802.3 standard.
   virtual function ENCODED_WORD_TYPE control_table(input byte word, input bit disparity);
      control_table = control_code_table[unsigned'(word)][disparity];
      if(control_table == '0)
	`uvm_fatal("SERDES_FATAL", "The given 8b word doesn't have a corresponding 10b control word")
   endfunction // control_table

   // Function: encoding_table_rev
   // Decodes ~code~ for a given ~disparity~. If ~code~ is doesn't exist '0 is returned.
   function WORD_TYPE encoding_table_rev(input ENCODED_WORD_TYPE code, input bit disparity);
      encoding_table_rev = code_table_rev[disparity][unsigned'(code)];
   endfunction // encoding_table_rev

   // Function: calculate_disparity
   // Calculate and return the new ~disparity~ basing on the given ~code~ and ~disparity~.
   virtual function bit calculate_disparity(input ENCODED_WORD_TYPE code, input bit disparity);
     
      calculate_disparity = disparity;
      	
      //first sub-block parity
      if( !disparity) begin //RD-
	 if( $countones(code[6:9]) > 2 || code[5:9] == 4'b0011 ) 
	   calculate_disparity = 1;
	 else if ($countones(code[6:9]) == 2) begin //neutral disparity of 3b/4b
	    if( $countones(code[0:5]) > 3 || code[0:5] == 6'b000111 )
	      calculate_disparity = 1;
	 end
      end
      else begin //RD+
	 if( $countones(code[6:9]) < 2 || code[5:9] == 4'b1100 )
	   calculate_disparity = 0;
	 else if ($countones(code[6:9]) == 2) begin //neutral disparity 3b/4b
	    if( $countones(code[0:5]) < 3 || code[0:5] == 6'b111000 )
	      calculate_disparity = 0;
	 end
      end // else: !if( !disparity)
   endfunction // calculate_disparity

   //Function: is_comma
   //The function return 1 if the given code is comma pattern.
   virtual function bit is_comma(input ENCODED_WORD_TYPE code);
      return (  ( (code & COMMA_MASK) == (COMMA_PATTERN_P & COMMA_MASK) ) || ( (code & COMMA_MASK) == (COMMA_PATTERN_N & COMMA_MASK) ) );
   endfunction // is_comma

   //Function: is_idle
   //The function return 1 if the given sequence is a proper IDLE pattern.
   //If ~error~ is 1, the internal assertion generates an error if K28.5 is followed by wrong data word.
   virtual function bit is_idle(input WORD_TYPE word, input int unsigned index, input bit error = 1 );
      is_idle = 0;
      case (index) 
	0 : begin
	   if( word == {1'b1, 8'hBC } ) //K28.5
	     is_idle = 1;
	end
	1 : begin
	   if( word == {1'b0, 8'hC5} || word == {1'b0, 8'h50} )  //D5.6 or D16.2
	     is_idle = 1;
	   else
	     if( error )
	       WRONG_IDLE_STATE: assert( 0 ) else `uvm_error( "EncodingTable_8b10b_802_3", $sformatf("Wrong idle state. K28.5 is followed by %h", word) );
	end
      endcase // case (index)
   endfunction // is_idle

endclass // EncodingTable_8b10b_802_3
