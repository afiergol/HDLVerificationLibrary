//                              -*- Mode: Verilog -*-
// Filename        : EncodingTable_8b10b.sv
// Description     : Specifies 8b10 encoding table.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 10 16:30:26 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

// Title: Encoding Tables
//
// Set of classes providing 8b10b encoding tables.

// Class: EncodingTable_8b10b
// The virtual class specifying interface of 8b/10b encoding tables.
virtual class EncodingTable_8b10b extends uvm_object;

   // Function: new
   // Creates a new <EncodingTable> with the given instance ~name~. 
   // If ~name~ is not supplied, the object is unnamed.
   function new(string name = "");
      super.new(name);
   endfunction // new
   
   `uvm_object_utils(  EncodingTable_8b10b )

   //Variable: IDLE_LENGTH
   //Length of the IDLE sequence.
   static int unsigned IDLE_LENGTH = 0;
   
   // Function: encoding_table
   //
   // Return an encoded ~word~ for the given ~disparity~.

   pure virtual function ENCODED_WORD_TYPE control_table(input byte word, input bit disparity);

   //Function: control_table
   //
   // Return an encoded control ~word~ for the given ~disparity~.

   pure virtual function ENCODED_WORD_TYPE encoding_table(input byte word, input bit disparity);

   // Function: encoding_table_rev
   //
   // Decodes ~code~ for a given ~disparity~. If ~code~ is incorrect, '1 is returned.
   
   pure virtual function WORD_TYPE encoding_table_rev(input ENCODED_WORD_TYPE code, input bit disparity);

   // Function: calculate_disparity
   //
   // Calculate and return the new ~disparity~ basing on the current ~code~ and ~disparity~.
   
   pure virtual function bit calculate_disparity(input ENCODED_WORD_TYPE code, input bit disparity);

   //Function: is_comma
   //The function return 1 if the given code is comma pattern.
   pure virtual function bit is_comma(input ENCODED_WORD_TYPE code);

   //Function: is_idle
   //The function return 1 if the given sequence is a proper IDLE pattern.
   pure virtual function bit is_idle(input WORD_TYPE word,  input int unsigned index, input bit error = 1 );
   
 endclass // encoding_table
