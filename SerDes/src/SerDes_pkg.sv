//                              -*- Mode: Verilog -*-
// Filename        : SerDes_pkg.sv
// Description     : Package providing SerDes interface.
// Author          : Adrian Fiergolski
// Created On      : Tue Mar  8 18:34:17 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifndef SERDES_PKG_SV
 `define  SERDES_PKG_SV

 `include "EncodingTable/EncodingTable_pkg.sv"
 `include "SerDes_inter.sv"

package SerDes_pkg;

   import uvm_pkg::*;
 `include <uvm_macros.svh>

   import BasicBlocks_pkg::*;
   import EncodingTable_pkg::*;
   
   //Typedef: vSerDes_inter
   typedef virtual interface SerDes_inter vSerDes_inter;

   export EncodingTable_pkg::WORD_TYPE;
   
 `include "SerDes_sequence_item.sv"
 `include "SerDes_config.sv"
 `include "SerDes_monitor.sv"
 `include "SerDes_monitor_8b10b.sv"
 `include "SerDes_driver.sv"
 `include "SerDes_sqr.sv"
 `include "SerDes_agent.sv"
   
endpackage // SerDes_pkg
     
`endif
