//                              -*- Mode: Verilog -*-
// Filename        : SerDes_monitor_8b10b.sv
// Description     : SerDes monitor.
// Author          : Adrian Fiergolski
// Created On      : Mon Mar 14 16:34:32 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Topic: SerDes_monitor_8b10b


//Class: SerDes_word_8b10b
//The class used to create stream of transactions.
class SerDes_word_8b10b extends uvm_sequence_item;

   //Variable: encoded
   ENCODED_WORD_TYPE sipo;

   //Variable: disparity
   bit disparity;

   //Variable: is_word
   bit is_control;
   
   //Variable: decoded
   byte decoded;
   
   `uvm_object_utils_begin(SerDes_word_8b10b)
      `uvm_field_int(sipo, UVM_DEFAULT | UVM_BIN)
      `uvm_field_int(disparity, UVM_DEFAULT | UVM_BIN )
      `uvm_field_int(is_control, UVM_DEFAULT)
      `uvm_field_int(decoded, UVM_DEFAULT | UVM_BIN)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <SerDes_word_8b10b> with the given ~name~.
   function new(string name="SerDes_word_8b10b");
      super.new(name);
   endfunction // new
   
endclass // SerDes_word_8b10b



//Class: SerDes_monitor_8b10b
//
//Behavioral implementation of the SerDes monitor. If follows the 802.3 specification.

class SerDes_monitor_8b10b extends SerDes_monitor;

   //Variable: disparity
   // Indicates the current disparity (~false~ indicates -, ~true~ indicates +)
   protected bit disparity = 0;

   //Variable: code_scheme
   //
   // The code tables.
   // The ~code_scheme~ object should be created in a parent component using set_config_object interface of UVM. 
   protected EncodingTable_8b10b_802_3 code_scheme;

   //Variable: state
   //Indicates whether the monitor is synchronised.
   protected enum { OUT_OF_SYNCH, BIT_ALIGNED, SYNCH} state = OUT_OF_SYNCH;

   //Variable: idle_index
   //Index of the idle pattern
   protected int unsigned idle_index;
   
   //Variable sipo
   //Serial-in parallel-out register for the link
   ENCODED_WORD_TYPE sipo;   

   `uvm_component_utils_begin( SerDes_monitor_8b10b )
      `uvm_field_object(code_scheme, UVM_ALL_ON | UVM_NOPRINT | UVM_NOPACK)
   `uvm_component_utils_end

   
   // Function: new
   //
   // Create a new <SerDes_monitor_8b10b> with the given instance ~name~ and parent. 
   // If ~name~ is not supplied, the monitor is unnamed.
   
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new   

   // Function: build_phase
   // Creates code scheme and recording streams.
   virtual function void  build_phase(uvm_phase phase);
      super.build_phase(phase);
      code_scheme = EncodingTable_8b10b_802_3::type_id::create("code_scheme");
   endfunction // build_phase
   

   // Task: synchronise
   // Task is performing synchronisation of the link.
   virtual task synchronise();

      do begin
	 do begin
	    collect_bits(1);
	 end
	 while( ! code_scheme.is_comma(sipo) );
	 
	 state = BIT_ALIGNED;
	 `uvm_info("SerDes_monitor", "Monitor is bit aligned. Found a proper comma in the stream. Waiting for synchronisation...", UVM_LOW)

	 byte_align();
      end
      while(state == SYNCH);
      
      `uvm_info("SerDes_monitor", "Monitor is synchronised.", UVM_LOW)
      state = SYNCH;
      
   endtask // synchronise


   //Task: byte_align
   virtual task byte_align();

      WORD_TYPE decoded;

      if( code_scheme.is_idle( code_scheme.encoding_table_rev(sipo, disparity), idle_index, 0) ) //is it already the first word of the IDLE
	idle_index++;
      
      disparity = code_scheme.calculate_disparity(sipo, disparity); //anyway calculate disparity
      
      do begin
	 collect_word( decoded );

	 if (decoded == '1) begin  //if wrong word
	    state = OUT_OF_SYNCH;  //start the whole synchronisation from scratch
	    return;
	 end
	 
	 if(  ! code_scheme.is_idle(decoded, idle_index++, 0) ) //if wrong IDLE pattern 
	   idle_index = 0;                                      //start from scratch
      end
      while( idle_index < code_scheme.IDLE_LENGTH ); //wait for the first valid IDLE pattern with a proper parity
      idle_index = 0;

   endtask // byte_align
   
   //Task: receive
   //The task collects transactions.
   task receive();
      do begin
	 if( state != SYNCH )
	   synchronise();
	 tr = SerDes_sequence_item::type_id::create("Transaction_monitor");
	 idle_state();
	 collect_item();
      end
      while(state != SYNCH ); //repeat till synchronisation
   endtask // receive

   // Task: collect_item
   // Collect the following transactions.
   virtual task collect_item();
      WORD_TYPE decoded;

      if(state != SYNCH) //if not in SYNCH
	return;          //exit
      
      forever begin
	 collect_word(decoded);
	 if(decoded == '1) begin  //lost synchronisation
	    `uvm_warning("SerDes_monitor", "SerDes lost synchronisation during data collection");
	    state = OUT_OF_SYNCH;
	    end_tr(tr);
	    return;
	 end

	 if( ! code_scheme.is_idle(decoded, 0, 0) )  //still data frame
	   push_back(decoded);
	 else begin
	    idle_index++;
	    end_tr(tr);
	    return;
	 end
      end // forever begin
   endtask // collect_item

   //Task: idle_state
   //The task handles IDLE state.
   virtual task idle_state();
      WORD_TYPE decoded;
      
      forever begin
	 collect_word( decoded );
	 if( ! code_scheme.is_idle(decoded, idle_index, 0) ) begin //if it's not an IDLE pattern
	    if( decoded == '1 || idle_index != 0 ) begin          //if not a valid code or fist frame word
	       `uvm_warning("SerDes_monitor", "SerDes lost synchronisation during IDLE pattern");
	       state = OUT_OF_SYNCH;
	    end
	    else if (idle_index == 0) begin //it the first word the of the transaction
	       void'( begin_tr(tr, "MonitorDataStream") );
	       push_back(decoded);
	    end
	    return;
	 end
	 idle_index++;
	 
	 if(idle_index == code_scheme.IDLE_LENGTH)     //modulo IDLE_LENGTH
	   idle_index = 0;
      end // forever begin
   endtask // idle_state
   
   // Task: collect_bits
   // Collect ~count~ number of bits in ~sipo~.
   task collect_bits(input byte unsigned count = 10);
      repeat(count) begin
	 @(inter.cbs);
	 sipo = {sipo, inter.cbs.link};
      end
   endtask // collect_bits

   // Task: collect_word
   // Collect code, decoded it and calculate the new disparity.
   virtual task collect_word(output WORD_TYPE decoded);

      SerDes_word_8b10b word_ = SerDes_word_8b10b::type_id::create("word_");
      
      void'(begin_tr(word_, "MonitorWordStream") );
      collect_bits( $size(ENCODED_WORD_TYPE) );
      
      decoded = code_scheme.encoding_table_rev(sipo, disparity);

      //record word_ transaction
      word_.sipo = sipo;
      word_.disparity = disparity;
      word_.decoded = decoded.word;
      word_.is_control = decoded.is_control;
      end_tr(word_);
      
      disparity = code_scheme.calculate_disparity(sipo, disparity);

      
   endtask // collect_word

   //Function: push_back
   //It push the word to the current transaction
   virtual function void push_back(input WORD_TYPE word);
      SerDes_sequence_item tr_;
      assert ($cast(tr_, tr));
      tr_.data.push_back(word);
   endfunction // push_back
   
endclass // SerDes_monitor_8b10b

