//                              -*- Mode: Verilog -*-
// Filename        : SerDes_driver.sv
// Description     : SerDes_driver.
// Author          : Adrian Fiergolski
// Created On      : Mon Mar 14 17:05:15 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: SerDes_driver
//SerDes driver.
class SerDes_driver extends uvm_push_driver #(SerDes_sequence_item);
   
   `uvm_component_utils_begin(SerDes_driver)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <SerDes_driver> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
      `uvm_error("SERDES_DRIVER", "SerDes driver is not implemented");
   endfunction // new
   
endclass // SerDes_driver
