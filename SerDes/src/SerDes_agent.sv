//                              -*- Mode: Verilog -*-
// Filename        : SerDes_agent.sv
// Description     : SerDes agent.
// Author          : Adrian Fiergolski
// Created On      : Mon Mar 14 17:01:37 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.


//Class: SerDes_agent
//SerDes Agent.
class SerDes_agent extends uvm_agent;

   //Variable: driver
   //The driver: <SerDes_driver>
   SerDes_driver driver;

   //Variable: sqr
   //The sequencer: <SerDes_sqr>.
   SerDes_sqr sqr;
   
   //Variable: monitor
   //The monitor: <SerDes_monitor>
   SerDes_monitor monitor;

   //Variable: cfg
   //SerDes configuration
   SerDes_config cfg;
   
   `uvm_component_utils_begin(SerDes_agent)
      `uvm_field_object(sqr, UVM_DEFAULT| UVM_REFERENCE)
      `uvm_field_object(driver, UVM_DEFAULT| UVM_REFERENCE)
      `uvm_field_object(monitor, UVM_DEFAULT| UVM_REFERENCE)
      `uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <SerDes_agent> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   // Function: build_phase
   // Implement UVM build_phase.
   // The function creates a driver, a monitor and a sequencer.
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);

      monitor = SerDes_monitor::type_id::create("monitor",this);

      if(cfg == null) begin
	 if( !uvm_config_db#(SerDes_config)::get(this, "", "cfg", cfg) ) begin
	   `uvm_warning("NOCONFIG", "SerDes_config not set for this component.")
	 end
      end
      else is_active = cfg.is_active;
      
      if(get_is_active() == UVM_ACTIVE) begin
	 driver = SerDes_driver::type_id::create("driver",this);
	 sqr = SerDes_sqr::type_id::create("sqr", this);
      end

      set_config_object("monitor", "monCfg", cfg.monCfg, .clone(0) );
   endfunction // build_phase

   // Function: connect_phase
   //
   // Implement UVM connect phase.
   // It connects driver with sequencer.
   
   virtual function void connect_phase(uvm_phase phase);
      if(get_is_active() == UVM_ACTIVE) begin
         sqr.req_port.connect(driver.req_export);
      end
   endfunction // connect_phase

   
endclass // SerDes_agent
