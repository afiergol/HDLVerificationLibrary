//                              -*- Mode: Verilog -*-
// Filename        : SerDes_sequence_item.sv
// Description     : Basic sequence item used by the SerDes package.
// Author          : Adrian Fiergolski
// Created On      : Mon Mar 14 16:57:04 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: SerDes_sequence_item
//Basic sequence item used by the SerDes package.
class SerDes_sequence_item extends uvm_sequence_item;

   //Variable: data
   WORD_TYPE data[$];
   
   `uvm_object_utils_begin(SerDes_sequence_item)
      `uvm_field_queue_int(data, UVM_DEFAULT | UVM_HEX | UVM_NORECORD)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <SerDes_sequence_item> with the given ~name~.
   function new(string name="SerDes_sequence_item");
      super.new(name);
   endfunction // new

endclass // SerDes_sequence_item

