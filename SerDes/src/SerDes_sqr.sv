//                              -*- Mode: Verilog -*-
// Filename        : SerDes_sqr.sv
// Description     : SerDes_sqr
// Author          : Adrian Fiergolski
// Created On      : Mon Mar 14 17:06:16 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

// Class: SerDes_sqr
// Implement generic UVM sequencer of the SerDes.

class SerDes_sqr extends uvm_push_sequencer #( SerDes_sequence_item );

   //Variable: cfg
   //SerDes configuration
   SerDes_config cfg;

   `uvm_component_utils_begin( SerDes_sqr )
      `uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end
   
   // Function: new
   //
   // Creates a new <SerDes_sqr> with the given instance ~name~ and parent. 
   // If ~name~ is not supplied, the driver is unnamed.
   
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new
   
   //Function: build_phase
   //
   //It looks for a configuration.
   function void build_phase(uvm_phase phase);
      if(cfg == null)
	if( !uvm_config_db#(SerDes_config)::get(this, "", "cfg", cfg) )
	  `uvm_warning("NOCONFIG", "SerDes_config not set for this component")
   endfunction // build_phase

endclass // SerDes_sqr
