//                              -*- Mode: Verilog -*-
// Filename        : SerDes_config.sv
// Description     : Config class for SerDes agent.
// Author          : Adrian Fiergolski
// Created On      : Mon Mar 14 17:10:35 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: SerDes configuration

//Class: SerDes_seq_config
//
//Basic class holding SerDes configuration of sequences.
class SerDes_seq_config extends uvm_object;
   
   `uvm_object_utils(SerDes_seq_config)

   //Function: new
   //
   //Creates a new <SerDes_seq_config> with the given ~name~
   function new(string name = "SerDes_seq_config");
      super.new(name);
   endfunction // new

endclass // SerDes_seq_config

//Class: SerDes_monitor_config
//The configuration class of the <SerDes_monnitor>
class SerDes_monitor_config extends monitor_base_config;
   
   `uvm_object_utils_begin(SerDes_monitor_config)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <SerDes_monitor_config> with the given ~name~.
   function new(string name="SerDes_monitor_config");
      super.new(name);
   endfunction // new

   //Variable: inter
   vSerDes_inter inter;
   
endclass // SerDes_monitor_config

//Class: SerDes_config
//
//Class holding SerDes configuration.
class SerDes_config extends uvm_object;

   //Variable: seqCfg
   //The configuration of the sequence
   SerDes_seq_config seqCfg;

   //Variable: monCfg
   //The configuration of the sequence
   SerDes_monitor_config monCfg;
   
   //Variable: inter
   vSerDes_inter inter;

   //Variable: is_active 
   //Specifies whether agent is active.
   rand uvm_active_passive_enum is_active = UVM_ACTIVE;  
   
   `uvm_object_utils_begin(SerDes_config)
      `uvm_field_enum(uvm_active_passive_enum, is_active, UVM_DEFAULT)
      `uvm_field_object(seqCfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_object_utils_end

   //Function: new
   //
   //Creates a new <SerDes_config> with the given ~name~ and instantiates <seqCfg>.
   function new(string name = "SerDes_config");
      super.new(name);
      seqCfg = SerDes_seq_config::type_id::create("SerDes sequence default config" );
      monCfg = SerDes_monitor_config::type_id::create("SerDes monitor default config");
   endfunction // new
   
endclass // SerDes_config
