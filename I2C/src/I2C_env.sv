//                              -*- Mode: Verilog -*-
// Filename        : I2C_env.sv
// Description     : I2C environment.
// Author          : Adrian Fiergolski
// Created On      : Wed Nov 11 17:29:50 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: I2C_env
//The class configures properly the mother <VIP_env> class.
class I2C_env extends VIP_env;
   
   `uvm_component_utils_begin(I2C_env)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <I2C_env> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   // Function: build_phase
   // It configures the instance to use <i2c_agent>,
   function void build_phase(uvm_phase phase);
      set_type_override_by_type(mvc_agent::get_type(), i2c_agent::get_type() );
      super.build_phase(phase);
   endfunction // build_phase

endclass // I2C_env
