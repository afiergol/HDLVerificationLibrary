//                              -*- Mode: Verilog -*-
// Filename        : i2c_unidir_bus_resolve.sv
// Description     : The module resolves state on SDA and SCL lines
//                   combining signals from all masters and slaves
//
// Author          : Adrian Fiergolski
// Created On      : Fri May 3 12:49:53 2019
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

// The module resolves state on SDA and SCL lines combining signals from all masters and slaves
//Inspired by Mentor's module.
module i2c_unidir_bus_resolver
  #(int NUM_MASTERS = 1,  //number of master
    int NUM_SLAVES = 1)   //number of slaves
   (
    input [NUM_MASTERS-1:0] master_SDA,
    input [NUM_MASTERS-1:0] master_SCL,
    input [NUM_SLAVES-1:0]  slave_SDA,
    input [NUM_SLAVES-1:0]  slave_SCL,
    output 		    SDA, // resolved SDA signal
    output 		    SCL          // resolved SCL signal
    );

   assign SCL = (& master_SCL) & (& slave_SCL);
   assign SDA = (& master_SDA) & (& slave_SDA);
   
endmodule: i2c_unidir_bus_resolver
