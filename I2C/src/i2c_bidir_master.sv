//                              -*- Mode: Verilog -*-
// Filename        : i2c_bidir_master.sv
// Description     : The module, using Mentor's BFM, emulates I2C master.
//
// Author          : Adrian Fiergolski
// Created On      : Fri May 3 12:49:53 2019
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.


// The module, using Mentor's BFM, emulates I2C master.
//Inspired by Mentor's module.
module i2c_bidir_master ( inout SDA,        // SDA signal
                          inout SCL, // SCL signal
                          input sample_clk  // Sample clock
                          );

   import uvm_pkg::*;

   parameter string 		PATH_NAME = "uvm_test_top";  
   parameter string 		IF_NAME = "null";
   
   mgc_i2c i2c_bfm(.isample_clk(sample_clk));

   assign i2c_bfm.sSDAH = SDA;
   assign i2c_bfm.sSCLH = SCL;
   
   assign SDA = i2c_bfm.mSDAH ? 1'bz : 1'b0;
   assign SCL = i2c_bfm.mSCLH ? 1'bz : 1'b0;
   
   initial begin
      uvm_config_db #(virtual mgc_i2c)::set(null, PATH_NAME, IF_NAME, i2c_bfm);
   end

endmodule: i2c_bidir_master
