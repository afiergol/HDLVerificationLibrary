//                              -*- Mode: Verilog -*-
// Filename        : I2C_pkg.sv
// Description     : I2C package.
// Author          : Adrian Fiergolski
// Created On      : Wed Apr  1 14:10:09 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifndef I2C_PKG
 `define I2C_PKG

 `include <mvc_macros.svh>
//The headers below contain packages and modules so can't be included in the SFP_pkg
`include <questa_mvc_svapi.svh>
`include <mvc_pkg.sv>
`include <mgc_i2c_v2_1_pkg.sv>
`include "i2c_bidir_slave.sv"
`include "i2c_bidir_master.sv"
`include "i2c_unidir_slave.sv"
`include "i2c_unidir_master.sv"
`include "i2c_unidir_bus_resolver.sv"


package I2C_pkg;
   
   import uvm_pkg::*;
`include <uvm_macros.svh>
   
   //I2C package (Questa VIP)
   import mgc_i2c_v2_1_pkg::*;
   import mvc_pkg::*;
   
   //Virtual type of the interface
   typedef virtual mgc_i2c vI2C;

`include "BasicBlocks_pkg.sv"
   import BasicBlocks_pkg::*;

   //Typedef: I2C_config
   typedef VIP_config I2C_config;

   //Typedef: I2C_virtual_sqr
   typedef VIP_virtual_sqr I2C_virtual_sqr;

`include "I2C_env.sv"
   
   export mvc_pkg::mvc_agent;
   export mvc_pkg::mvc_sequencer;
   export mvc_pkg::mvc_item_listener;
   export mvc_pkg::mvc_config_base_id;
   export mgc_i2c_v2_1_pkg::i2c_agent;
   export mgc_i2c_v2_1_pkg::i2c_vip_config;
   export mgc_i2c_v2_1_pkg::i2c_master_i2c_data_transfer;
   export mgc_i2c_v2_1_pkg::i2c_slave_i2c_data_transfer;
   export mgc_i2c_v2_1_pkg::i2c_master_base_sequence;
   export mgc_i2c_v2_1_pkg::I2C_MASTER;
   export mgc_i2c_v2_1_pkg::I2C_SLAVE;
   export mgc_i2c_v2_1_pkg::I2C_SEVEN_BIT_ADDRESS;
   export mgc_i2c_v2_1_pkg::I2C_TEN_BIT_ADDRESS;
   export mgc_i2c_v2_1_pkg::I2C_GENERAL_CALL;
   export mgc_i2c_v2_1_pkg::I2C_HS_MODE;
   export mgc_i2c_v2_1_pkg::I2C_START_BYTE;
   export mgc_i2c_v2_1_pkg::I2C_CBUS;
   export mgc_i2c_v2_1_pkg::I2C_RESERVED_BUS_FORMAT;
   export mgc_i2c_v2_1_pkg::I2C_RESERVED_ADDRESS;
   export mgc_i2c_v2_1_pkg::I2C_DEVICE_ID_7B_ADDR;
   export mgc_i2c_v2_1_pkg::I2C_DEVICE_ID_10B_ADDR;
   export mgc_i2c_v2_1_pkg::i2c_request_type_e;
   export mgc_i2c_v2_1_pkg::i2c_slave_sequence;
   export mgc_i2c_v2_1_pkg::I2C_GLITCH_SUPPORT_DISABLED;
   export mgc_i2c_v2_1_pkg::I2C_GLITCH_AFTER_START_CONDITION;
   export mgc_i2c_v2_1_pkg::I2C_GLITCH_DURING_ADDRESS_BYTE;
   export mgc_i2c_v2_1_pkg::I2C_GLITCH_DURING_DATA_BYTE;
   export BasicBlocks_pkg::VIP_monitor_config;
   
endpackage // I2C_pkg
   
`endif
  
