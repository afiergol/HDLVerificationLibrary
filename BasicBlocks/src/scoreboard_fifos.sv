//                              -*- Mode: Verilog -*-
// Filename        : scoreboard_fifos.sv
// Description     : Generic scoreboard receiving transactions and putting them in UVM_TLM_ANALYSIS_FIFOs.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:31:19 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: scoreboard_fifos
//Generic scoreboard receiving packets from ports and putting them in UVM TLM FIFOs.
//This class should be a base class for scoreboards using i.e. two input analysis ports. One needs to overload the task ~run_phase~ and implement a required checks.
class scoreboard_fifos extends scoreboard_base;

   //Variable: fifo
   //Associative array of FIFOs collecting incoming data from the agents.
   protected uvm_tlm_analysis_fifo #(uvm_sequence_item) fifo[string];

   `uvm_component_utils( scoreboard_fifos )

   //Function: new
   //Creates a new <scoreboard_fifos> with the given instance ~name~.
   function new( string name="", uvm_component parent );
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //The function creates required analysis ports: ~mac_port~ and ~phy_port~. 
   function void build_phase(uvm_phase phase);
      super.build_phase(phase);

      foreach( ports[ name ] )
	fifo[ name ] = new( {name, "_fifo"}, this);

   endfunction // build_phase

   //Function: connect_phase
   //The function connects the analysis ports with FIFOs
   function void connect_phase(uvm_phase phase);
      super.connect_phase(phase);

      foreach( ports[ name ] )
	ports[ name ].connect( fifo[name].analysis_export );

   endfunction // connect_phase

   //Task: run_phase
   //It gets the data from the fifo and send them to the receive function
   virtual task run_phase(uvm_phase phase);
      foreach( fifo[name] )
	fork
	   automatic string thread_name = name;
	   begin
	      uvm_sequence_item transaction_;
	      forever begin
		 fifo[thread_name].get(transaction_);
		 receive(thread_name, transaction_);
	      end
	   end
	join_none
   endtask // run_phase

   //Task: receive
   //Task receiving ~transaction_~ from the given ~port~.
   //Should be overloaded by the inheriting classes
   virtual task receive(string port, uvm_sequence_item transaction_);
	`uvm_error("PORT ERROR", $sformatf("No %s port has been found to serve the transaction", port) )
   endtask // receive
   
endclass // scoreboard_fifos
