//                              -*- Mode: Verilog -*-
// Filename        : monitor_base.sv
// Description     : The generic monitor.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:30:19 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: Generic monitor
//This part describes a generic  monitor which can be used by user as a base class.

//Class: monitor_base_config
//The configuration class of the <monitor_base>.
class monitor_base_config extends uvm_object;

   `uvm_object_utils_begin(monitor_base_config)
   `uvm_object_utils_end

   //Variable: ap_enabled
   //The variable indicates whether analysis port should be created for the given monitor (by default yes).
   //It allows to overcome connection error, when only peek port of the monitor is used.
   bit ap_enabled = 1;
   
   //Function: new
   function new(string name="monitor_base_config");
      super.new(name);
   endfunction

endclass // monitor_base_config

//Class: monitor_base
//The generic monitor.
//The monitor sends received transactions via analysis port <ap> to scoreboards and allows to peek transaction through <peekPort> (useful for sequences requiring access).
virtual class monitor_base extends uvm_monitor;

   //Variable: tr
   //The received transaction.
   protected uvm_sequence_item tr;

   //Variable: tr_copy
   //Copy of last received <tr>.
   //For safety, should be used by connected components (scoreboards, ect.)
   local uvm_sequence_item tr_copy;
   
   //Variable: monCfg
   //The configuration.
   monitor_base_config monCfg;

   //Variable: ap
   //Analysis port
   uvm_analysis_port #( uvm_sequence_item ) ap;

   //Variable: packetGrabbed
   //Event used to inform <peek> method about new packet.
   protected event packetGrabbed;
   
   //Variable: peekPort
   //The port to peek packet.
   //In case the sequence needs to see a packet.
   uvm_blocking_peek_imp #( uvm_sequence_item, monitor_base) peekPort;
   
   `uvm_component_utils_begin(monitor_base)
      `uvm_field_object(monCfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end

   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction

   //Function: build_phase
   //It fetches configuration and creates ports
   function void build_phase( uvm_phase phase );

      uvm_object temp;
      
      super.build_phase(phase);
      
      if( monCfg == null )
	`uvm_fatal("NOCONFIG", $sformatf("No configuration set for the %s", get_name() ) );

      if( monCfg.ap_enabled )
	ap = new( { get_name(), "_ap"}, this);
      
      peekPort = new( {get_name(), "_peek"}, this);
      
   endfunction // build_phase

   //Task: run_phase
   //It calls <receive> and <announce_tr> subsequently in forever loop.
   virtual task run_phase(uvm_phase phase);
      forever begin
	 receive();
	 announce();
      end
   endtask // run_phase

   //Function: announce_tr
   //Function announces transaction on its ports.
   virtual function void announce();
      assert( $cast(tr_copy, tr.clone() ) );
      -> packetGrabbed;
      if(ap != null )
	ap.write( tr_copy );
   endfunction // announce

   //Task: receive
   //It receives transaction from the bus.
   //User should overwrite this task.
   pure virtual task receive();

   //Task: peek
   //It is used by sequences to peek packets.
   task peek(output uvm_sequence_item trans);
      @packetGrabbed;
      trans = tr_copy;
   endtask // peek

endclass // monitor_base
