//                              -*- Mode: Verilog -*-
// Filename        : scoreboard_base.sv
// Description     : The base classes for scoreboards implementations.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:30:53 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: Base scoreboard

//Class: scoreboard_base_config
//The base class for configurations of the sub-scoreboards.
class scoreboard_base_config extends uvm_object;

   //Variable: port_names
   //Variable with names of ports to be created.
   string port_names[$];
   
   `uvm_object_utils( scoreboard_base_config )

   //Function: new
   //Creates a new <scoreboard_base_config> with the given instance ~name~.
   function new( string name = "scoreboard_base_config" );
      super.new( name );
   endfunction // new

   //Function: add_port
   //The method adds name of the port to be crated
   function void add_port(string name);
      string temp[$] = port_names.find_first() with ( item == name );
      
      if( temp.size() == 0 )
	 port_names.push_back( name );
      else
	`uvm_info("PORT_EXISTS", $sformatf("Port '%s' already exists in '%s'", name, get_name()), UVM_LOW);

   endfunction // add_port
   
endclass // scoreboard_base_config

//Class: scoreboard_base
//The base class for the scoreboards.
class scoreboard_base extends uvm_scoreboard;

   //Variable: ports
   //Associative array of ports exported by the scoreboard.
   uvm_analysis_export #(uvm_sequence_item) ports[string];

   //Variable: cfg
   //Configuration of the scoreboard.
   scoreboard_base_config cfg;
   
   `uvm_component_utils( scoreboard_base )

   // Function: new
   // Creates a new <scoreboard_base> with the given instance ~name~ and parent. 
   function new(string name = "", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //It looks for the configuration <cfg> and basing on it, the method creates ports.
   function void build_phase(uvm_phase phase);
      uvm_object cfg_;

      super.build_phase(phase);

      //Set configuration
      if( get_config_object("cfg", cfg_, 0) )
	assert( $cast(cfg, cfg_) );

      if(cfg == null) 
	`uvm_error("NOCONFIG", $sformatf("No configuration set for the %s", get_name() ) );

      //Create ports basing on configuration
      foreach( cfg.port_names[i] ) begin
	 if( ! ports.exists( cfg.port_names[i] ) )
	   ports[ cfg.port_names[i] ] = new( cfg.port_names[i], this );
	 else
	   `uvm_warning("WRONG_CFG", $sformatf("The port %s already exists", cfg.port_names[i] ) );
      end

   endfunction // build_phase

   //Function: report_phase
   //It reports statistics.
   //This function prints only type of the scoreboard and should be defined in all inheriting classes. 
   virtual function void report_phase(uvm_phase phase);
      super.report_phase(phase);
      
      //Print statistics
      `uvm_info( get_type_name(),
		 "-------------------------------- Scoreboard report --------------------------------",
		 UVM_MEDIUM );
   endfunction // report_phase
   
endclass // scoreboard_base


