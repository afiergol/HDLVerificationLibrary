//                              -*- Mode: Verilog -*-
// Filename        : scoreboard_main_base.sv
// Description     : The generic scoreboard which contains sub-scoreboards.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:32:03 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: scoreboad_main_base

//Class: subscoreboard_config
//It stores information about subscoreboard to by created inside the scoreboard_main_base
class subscoreboard_config;
   //Variable: scoreboard_wrapper
   //Wrapper of the subscoreboard to be created by the factory.
   uvm_object_wrapper scoreboard_wrapper;

   //Variable: cfg
   //Configuration of the subscoreboard.  
   scoreboard_base_config cfg;

   //Typedef: port_assingment_config
   //The structure containing port assignment.
   typedef struct {
      //Variable: source_port
      string 	  source_port;
      //Variable: destination_port
      string 	  destination_port;
   } port_assingment_config;

   //Variable: ports
   //Port mapping.
   port_assingment_config ports[];

endclass // subscoreboard_config

//Class: scoreboard_main_base_config
//Configuration of the <scoreboard_main_base>
class scoreboard_main_base_config extends scoreboard_base_config;

   //Variable: scoreboard_wrappers
   //Associative array of sub-scoreboards to be build by the <scoreboard_main_base>.
   subscoreboard_config sub_scoreboards[string];


   `uvm_object_utils( scoreboard_main_base_config )

   typedef subscoreboard_config::port_assingment_config port_assingment_config;
   
   //Function: new
   //Creates a new <scoreboard_main_base_config> with the given instance ~name~.
   function new( string name = "scoreboard_main_base_config" );
      super.new( name );
   endfunction // new

   //Function: add_subscoreboard
   //It adds a subscoreboard  to the configuration.
   //The function takes dynamic array as arguments with port assignments and configures  the <sub-scoreboard> respectively.
   //Parameters:
   //- ~name~ - name of the subscoreboard to be created
   //- ~wrapper~ - type of the subscoreboard to be created
   //- ~cfg~ - configuration of the subscoreboard
   //- ~ports~ - ports associations on the mac_side
   function void add_subscoreboard_array(string name, uvm_object_wrapper wrapper, scoreboard_base_config cfg,
					  port_assingment_config ports[] );
      subscoreboard_config temp = new;

      temp.scoreboard_wrapper = wrapper;
      temp.ports = ports;
      
      if(cfg == null) begin
	 `uvm_warning("NO_CONFIG", 
		      $sformatf( "The configuration of the sub-scoreboard '%s' is missing. Creating a default one.", name));
	 cfg = scoreboard_base_config::type_id::create( {name, "_cfg"} );
      end
      temp.cfg = cfg;

      //add ports to the configuration
      foreach(ports[i]) begin
	 add_port(ports[i].source_port);
	 
	 //add port in the sub-scoreboard
	 cfg.add_port(ports[i].destination_port);
      end

      sub_scoreboards[name] = temp;
   endfunction // add_subscoreboard

   //Function: add_subscoreboard
   //It adds the subscoreboard to the configuration which has two ports.
   //The functions takes string names and calls <add_subscoreboard_arrays>.
   //Parameters:
   //- ~name~ - name of the subscoreboard to be created
   //- ~wrapper~ - type of the subscoreboard to be created
   //- ~cfg~ - configuration of the subscoreboard
   //- ~port1_source~ - name of the port on mac side connected to the ~port1_scr~
   //- ~port1_dest~ - first port of the scoreboard
   //- ~port2_source~ - name of the port on phy side connected to the ~port2_scr~
   //- ~port2_dest~ - second port of the scoreboard

   function void add_subscoreboard(string name, uvm_object_wrapper wrapper, scoreboard_base_config cfg, 
				   string port1_source = "", string port1_dest = "",
				   string port2_source = "", string port2_dest = "" );

      port_assingment_config ports[];
      int 				  size;
      int 				  index;
      
      if( (port1_source.len() != 0 && port1_dest.len() == 0) ||
	  (port1_source.len() == 0 && port1_dest.len() != 0) ) 
	`uvm_warning("CFG_ERROR", "One side of the port1 is not defined");
      
      if( (port2_source.len() != 0 && port2_dest.len() == 0) ||
	  (port2_source.len() == 0 && port2_dest.len() != 0) )
	`uvm_warning("CFG_ERROR", "One side of the port2 is not defined");

      if(port1_source.len() != 0 && port1_dest.len() != 0)
	size++;
      if(port2_source.len() != 0 && port2_dest.len() != 0)
	size++;

      ports = new[size];
      
      if(port1_source.len() != 0 && port1_dest.len() != 0) begin
	 ports[index++] = '{ port1_source, port1_dest};
      end

      if(port2_source.len() != 0 && port2_dest.len() != 0) begin
	 ports[index] = '{ port2_source, port2_dest};
      end
	 
      add_subscoreboard_array(name, wrapper, cfg, ports);
   endfunction // add_subscoreboard
   
endclass // scoreboard_main_base_config

//Class: scoreboard_main_base
//The base class to build main scoreboards (one per environment).
//The main scoreboard contains sub-scoreboards and connects them with the environment.
class scoreboard_main_base extends scoreboard_base;

   //Variable: scoreboards
   //Associative array of the scoreboards
   scoreboard_base scoreboards[string];

   `uvm_component_utils_begin(scoreboard_main_base)
   `uvm_component_utils_end

   //Function: new
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   extern virtual function void build_phase(uvm_phase phase);
   extern virtual function void connect_phase(uvm_phase phase);
   
endclass // scoreboard_main_base


//Function: build_phase
//It builds the scoreboards according to the <cfg>.
function void scoreboard_main_base::build_phase(uvm_phase phase);

   scoreboard_main_base_config _cfg;
   subscoreboard_config configs[string];
   uvm_factory f = uvm_factory::get();
      
   super.build_phase(phase);
   
   if( ! $cast(_cfg, cfg) )
     `uvm_error("NOCONFIG", "The component requires a dedicated type of configuration object (scoreboard_main_base_config)");
   
   if( _cfg == null ) begin
      `uvm_warning( "NOCONFIG", "There is no configuration, creating the default one");
      _cfg = scoreboard_main_base_config::type_id::create("cfg");
      cfg = _cfg;
   end
   
   configs = _cfg.sub_scoreboards;

   foreach( configs[name] ) begin
      uvm_component temp;
      
      `uvm_info( "Creating sub-scoreboard", name, UVM_HIGH)

      temp = f.create_component_by_type( configs[name].scoreboard_wrapper,
					 get_full_name(),
					 name,
					 this);
      if( ! $cast(scoreboards[name], temp) )
	`uvm_error("CAST_ERROR", "Wrong sub_scoreboard handle");

      //Assign configuration
      if( configs[name].cfg != null )
	set_config_object( name, "cfg", configs[name].cfg, .clone(0) );
   end
endfunction // build_phase

//Function: connect_phase
//It connects the sub_scoreboards with the ports of the <scoreboard_main_based>.
//User should overload this function if this deriving class requires special treatment.
function void scoreboard_main_base::connect_phase(uvm_phase phase);
   scoreboard_main_base_config _cfg;
   subscoreboard_config configs[string];

   super.connect_phase(phase);
   
   if( ! $cast(_cfg, cfg) )
     `uvm_error("NOCONFIG", "The component requires a dedicated type of configuration object (scoreboard_main_base_config)");

   configs = _cfg.sub_scoreboards;

   foreach( scoreboards[name] ) begin

      //Connect ports
      foreach( configs[name].ports[i] ) begin
	 string source_name =  configs[name].ports[i].source_port;
	 `uvm_info("connecting analysis port", 
		   $sformatf("%s -> %s", source_name, 
			     scoreboards[name].ports[ configs[name].ports[i].destination_port].get_full_name() ),
		   UVM_MEDIUM);
	 ports[ source_name ].connect( scoreboards[name].ports[ configs[name].ports[i].destination_port ]);
	 
      end
   end
   
endfunction // connect_phase

