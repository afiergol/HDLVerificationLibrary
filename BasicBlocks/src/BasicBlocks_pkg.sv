//                              -*- Mode: Verilog -*-
// Filename        : BasicBlocks_pkg.sv
// Description     : The package of basic blocks useful for building a testbench.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:29:56 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifndef BASICBLOCKS_PKG_SV
 `define BASICBLOCKS_PKG_SV

//Package: BasicBlocks_pkg
//Package contains basic blocks useful to build quickly a testbench.

package BasicBlocks_pkg;

   import uvm_pkg::*;
 `include <uvm_macros.svh>
   
 `include "scoreboard_base.sv"
 `include "scoreboard_fifos.sv"
 `include "scoreboard_main_base.sv"

 `include "monitor_base.sv"
   
endpackage // BasicBlocks_pkg

`endif
