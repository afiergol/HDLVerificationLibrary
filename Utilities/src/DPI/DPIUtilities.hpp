// Filename        : DPIUtilities.cpp
// Description     : Common DPI code.
// Author          : Adrian Fiergolski
// Created On      : Thu Mar 26 11:00:02 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

#ifndef DPIUTILITIES_HPP
#define DPIUTILITIES_HPP

//task in SV which enables progress in simulation time
extern int svSleep(long long);

//Throwed and the coresponding C++ task has been disabled
class taskDisabled : public std::exception
{
  virtual const char* what() const throw (){
    return "The coressponding System Verilog thread has been disabled";
  }
};

#endif
