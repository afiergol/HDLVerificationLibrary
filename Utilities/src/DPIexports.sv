//                              -*- Mode: Verilog -*-
// Filename        : DPIexports.sv
// Description     : SV functions and tasks called by the DPI C/C++.
// Author          : Adrian Fiergolski
// Created On      : Thu Apr 16 15:54:36 2015
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2015
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Task: sleep
//Used by bridge on C side to progress in SV time simulation.
task svSleep(time delay);
   # (delay);
endtask // sleep

export "DPI-C" task svSleep;
